import glob,sys
import numpy         as     np
from   igakit.igalib import bsp,iga
from   igakit.io     import PetIGA, VTK
from   scipy         import stats
from   igakit.nurbs  import NURBS
from   colorama      import Fore, Back, Style 
#
#######################################################################################
# Read FE point value
def readfiles(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	print "file "+namefile+" correctly read: "+Fore.GREEN+"yes"+Fore.RESET
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[12]));
		Hzzabs.append(float(line.split()[14]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)
#
#######################################################################################
# Read Fortran data for homogenous 3D case
def readfiles2(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	print "file "+namefile+" correctly read: "+Fore.GREEN+"yes"+Fore.RESET
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[11]));
		Hzzabs.append(float(line.split()[12]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)
#
#######################################################################################
# Read data
d      = int(sys.argv[1]) # dimension
Nkn    = int(sys.argv[2]) # Number on knots
p      = int(sys.argv[3]) # polynomial degree
#
nlevel = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,...
nbetas = int(sys.argv[5]) # Fourier discretization of the y direction (number of betas) e.g., 0,1,2,...
npnts  = int(sys.argv[6]) # Number of receivers (per subdomain) e.g., 1,2,3,...
nsegt  = int(sys.argv[7]) # Number of subdomains e.g., 1,2,3,...
#
srcid  = int(sys.argv[8]) # Source case e.g., 1,2,3,...
#
#######################################################################################
dirf     = "Hzz/"
namef    = "Hzz_"
middlef  = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(0)+"_sg"+str(nsegt)+"_Pos"+str(npnts)+"_Src"+str(srcid)
extf     = ".txt"
filename = dirf+namef+middlef+extf 
point,srcloc,srcmag,HzzRe,HzzIm,Hzzabs = readfiles(filename)
#
ReH=HzzRe
#
ImH=HzzIm
#
for j in range(1,nbetas+1):
	dirf     = "Hzz/"
	namef    = "Hzz_"
	middlef  = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(j)+"_sg"+str(nsegt)+"_Pos"+str(npnts)+"_Src"+str(srcid)
	extf     = ".txt"
	filename = dirf+namef+middlef+extf 
	point,srcloc,srcmag,HzzRe,HzzIm,Hzzabs = readfiles(filename)
	ReH=ReH+2.0*HzzRe
	ImH=ImH+2.0*HzzIm
#	
print "Points = "+str(point)
print "ReH = "+str(ReH)
print "ImH = "+str(ImH)
#
#######################################################################################
dirf     = "3D_homogeneous/"
namef    = "Hzz_fortran_sigma_"
middlef  = "0.1"
extf     = ".txt"
filename = dirf+namef+middlef+extf 
point,srcloc,srcmag,HzzRe,HzzIm,Hzzabs = readfiles2(filename)
# 
print "Points_3D_homogeneous = "+str(point.T[1][1:101])
print "ReH_3D_homogeneous = "+str(HzzRe)
print "ImH_3D_homogeneous = "+str(HzzIm) 