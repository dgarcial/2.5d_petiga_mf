import glob,sys
import numpy         as     np
from   igakit.igalib import bsp,iga
from   igakit.io     import PetIGA, VTK
from   scipy         import stats
from   igakit.nurbs  import NURBS
from   colorama      import Fore, Back, Style 
from   pyvtk2        import *

########################### - Read from terminal - ############################

# Read data
d      = int(sys.argv[1]) # dimension
Nkn    = int(sys.argv[2]) # Number on knots
p      = int(sys.argv[3]) # polynomial degree
#
nlevel = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,...
nbetas = int(sys.argv[5]) # Fourier discretization of the y direction (number of betas) e.g., 0,1,2,...
npnts  = int(sys.argv[6]) # Number of receivers (per subdomain) e.g., 1,2,3,...
nsegt  = int(sys.argv[7]) # Number of subdomains e.g., 1,2,3,...
#
srcid  = int(sys.argv[8]) # Source case e.g., 1,2,3,...

########################### - Read from file - ############################

# Loop of subdomains
for sg in range(1,nsegt+1):
	# Read mesh files
	ReHx, ImHx, ReHy, ImHy, ReHz, ImHz = ([] for i in range(6))
	#
	nrbHxRe, nrbHxIm, nrbHyRe, nrbHyIm, nrbHzRe, nrbHzIm = ([] for i in range(6))
	#
	print 'Case with n='+str(Nkn)+", p="+str(p)+", l="+str(nlevel)+", nbetas="+str(nbetas)+", npoints="+str(npnts)+", nseg="+str(nsegt)
	nrbHx = PetIGA(scalar='complex').read("geometry/grid_n"+str(Nkn)+"-p"+str(p)+"-c"+str(p-1)+"-l"+str(nlevel)+"-sg"+str(sg)+"-Hx.dat")
	print 'Mesh file of Hx (Hcurl): '+Fore.GREEN +'correctly read'+Fore.RESET
	nrbHy = PetIGA(scalar='complex').read("geometry/grid_n"+str(Nkn)+"-p"+str(p)+"-c"+str(p-1)+"-l"+str(nlevel)+"-sg"+str(sg)+"-Hy.dat")	
	print 'Mesh file of Hy (H1)   : '+Fore.GREEN +'correctly read'+Fore.RESET
	nrbHz = PetIGA(scalar='complex').read("geometry/grid_n"+str(Nkn)+"-p"+str(p)+"-c"+str(p-1)+"-l"+str(nlevel)+"-sg"+str(sg)+"-Hz.dat")
	print 'Mesh file of Hz (Hcurl): '+Fore.GREEN +'correctly read'+Fore.RESET
	nrbG  = PetIGA(scalar='complex').read("geometry/grid_n"+str(Nkn)+"-p"+str(p)+"-c"+str(p-1)+"-l"+str(nlevel)+"-sg"+str(sg)+"-geo.dat")
	print 'Mesh file of geometry  : '+Fore.GREEN +'correctly read'+Fore.RESET
	# Loop of points (solutions)
	for i in range(1,npnts+1):
		# Loop of betas
		# Reat results in x axis
		nameresult = "results/HX_N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt0_sg"+str(sg)+"_Pos"+str(i)+"_Src"+str(srcid)+".dat"
		print 'Solution of Hx (Hcurl) (beta=0): '+Fore.GREEN +'correctly read'+Fore.RESET
		Hx = PetIGA(scalar='complex').read_vec(nameresult,nrbHx)
		# Read results in y axis
		nameresult = "results/HY_N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt0_sg"+str(sg)+"_Pos"+str(i)+"_Src"+str(srcid)+".dat"
		print 'Solution of Hy (H1)    (beta=0): '+Fore.GREEN +'correctly read'+Fore.RESET
		Hy = PetIGA(scalar='complex').read_vec(nameresult,nrbHy)
		# Read results in z axis
		nameresult = "results/HZ_N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt0_sg"+str(sg)+"_Pos"+str(i)+"_Src"+str(srcid)+".dat"
		print 'Solution of Hz (Hcurl) (beta=0): '+Fore.GREEN +'correctly read'+Fore.RESET
		Hz = PetIGA(scalar='complex').read_vec(nameresult,nrbHz)		
		for j in range(1,nbetas+1):
			# Reat results in x axis
			nameresult = "results/HX_N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(j)+"_sg"+str(sg)+"_Pos"+str(i)+"_Src"+str(srcid)+".dat"
			print 'Solution of Hx (Hcurl) (beta='+str(j)+'): '+Fore.GREEN +'correctly read'+Fore.RESET
			Hx += PetIGA(scalar='complex').read_vec(nameresult,nrbHx)
			# Read results in y axis
			nameresult = "results/HY_N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(j)+"_sg"+str(sg)+"_Pos"+str(i)+"_Src"+str(srcid)+".dat"
			print 'Solution of Hy (H1)    (beta='+str(j)+'): '+Fore.GREEN +'correctly read'+Fore.RESET
			Hy += PetIGA(scalar='complex').read_vec(nameresult,nrbHy)
			# Read results in z axis
			nameresult = "results/HZ_N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(j)+"_sg"+str(sg)+"_Pos"+str(i)+"_Src"+str(srcid)+".dat"
			print 'Solution of Hz (Hcurl) (beta='+str(j)+'): '+Fore.GREEN +'correctly read'+Fore.RESET
			Hz += PetIGA(scalar='complex').read_vec(nameresult,nrbHz)
		# 
		# Organizing data in lists
		id = i-1
		# Hx
		ReHx.append(Hx.real)
		ImHx.append(Hx.imag)
		nrbHxRe.append(NURBS(nrbHx.knots, nrbHx.control, ReHx[id]))
		nrbHxIm.append(NURBS(nrbHx.knots, nrbHx.control, ImHx[id]))
		# Hy
		ReHy.append(Hy.real)
		ImHy.append(Hy.imag)
		nrbHyRe.append(NURBS(nrbHy.knots, nrbHy.control, ReHy[id]))
		nrbHyIm.append(NURBS(nrbHy.knots, nrbHy.control, ImHy[id]))
		# Hz
		ReHz.append(Hz.real)
		ImHz.append(Hz.imag)
		nrbHzRe.append(NURBS(nrbHz.knots, nrbHz.control, ReHz[id]))
		nrbHzIm.append(NURBS(nrbHz.knots, nrbHz.control, ImHz[id]))

	########################### - Imposing the mapping - ############################

	# Discretization to print the results
	#
	ugd = nrbG.degree
	ug  = nrbG.knots[0][ugd[0]:-ugd[0]]
	vg  = nrbG.knots[1][ugd[1]:-ugd[1]]
	#
	ufd = nrbHx.degree 
	uf  = nrbHx.knots[0][ufd[0]:-ufd[0]]
	vf  = nrbHx.knots[1][ufd[1]:-ufd[1]]
	#
	# Create fields from FEM result
	HxRe, HxIm, HyRe, HyIm, HzRe, HzIm = ([] for i in range(6))
	gradHxRe, gradHyRe, gradHzRe, gradHxIm, gradHyIm, gradHzIm = ([] for i in range(6))
	# Loop of points (solutions)
	for i in range(npnts):
		HxRe.append(nrbHxRe[i](ug,vg,fields=True)[1])
		HxIm.append(nrbHxIm[i](ug,vg,fields=True)[1])
		HyRe.append(nrbHyRe[i](ug,vg,fields=True)[1])
		HyIm.append(nrbHyIm[i](ug,vg,fields=True)[1])
		HzRe.append(nrbHzRe[i](ug,vg,fields=True)[1])
		HzIm.append(nrbHzIm[i](ug,vg,fields=True)[1])
		#
		HxRe[i] = HxRe[i][...,0]
		HyRe[i] = HyRe[i][...,0]
		HzRe[i] = HzRe[i][...,0]
		HxIm[i] = HxIm[i][...,0]
		HyIm[i] = HyIm[i][...,0]
		HzIm[i] = HzIm[i][...,0]
		#
		gradHxRe.append(nrbHxRe[i].gradient(None,ug,vg))
		gradHyRe.append(nrbHyRe[i].gradient(None,ug,vg))
		gradHzRe.append(nrbHzRe[i].gradient(None,ug,vg))
		gradHxIm.append(nrbHxIm[i].gradient(None,ug,vg))
		gradHyIm.append(nrbHyIm[i].gradient(None,ug,vg))
		gradHzIm.append(nrbHzIm[i].gradient(None,ug,vg))
	# Mappings
	F = nrbG.gradient(nrbG.points[...,:2], ug, vg)
	XYZ = nrbG(ug,vg,fields=False)
	#
	# For the state variable
	F00 = F[...,0,0]
	F01 = F[...,0,1]
	F10 = F[...,1,0]
	F11 = F[...,1,1]
	J = F00*F11-F10*F01
	invJ = 1.0/J
	invJ[np.isinf(invJ)] = 0
	#
	G00 = invJ * F11
	G01 = invJ * F01; G01 *= -1
	G10 = invJ * F10; G10 *= -1
	G11 = invJ * F00
	# Magnetic field 
	uuRe = G00*HxRe+G01*HzRe
	vvRe = HyRe
	wwRe = G10*HxRe+G11*HzRe
	uuIm = G11*HxIm+G01*HzIm
	vvIm = HyIm
	wwIm = G10*HxIm+G11*HzIm
	#
	Hx_xRe, Hx_zRe, Hz_xRe, Hz_zRe = ([] for i in range(4))
	Hx_xIm, Hx_zIm, Hz_xIm, Hz_zIm = ([] for i in range(4))
	Hy_xRe, Hy_zRe, Hy_xIm, Hy_zIm = ([] for i in range(4))
	# Loop of points (solutions)
	for i in range(npnts):
		Hx_xRe.append(gradHxRe[i][...,0,0])
		Hx_zRe.append(gradHxRe[i][...,0,1])
		Hz_xRe.append(gradHzRe[i][...,0,0])
		Hz_zRe.append(gradHzRe[i][...,0,1])
		Hx_xIm.append(gradHxIm[i][...,0,0])
		Hx_zIm.append(gradHxIm[i][...,0,1])
		Hz_xIm.append(gradHzIm[i][...,0,0])
		Hz_zIm.append(gradHzIm[i][...,0,1])
		#
		Hy_xRe.append(gradHyRe[i][...,0,0])
		Hy_zRe.append(gradHyRe[i][...,0,1])
		Hy_xIm.append(gradHyIm[i][...,0,0])
		Hy_zIm.append(gradHyIm[i][...,0,1])
	#
	# For the gradient
	DHx_zRe = G00*(G00*Hx_xRe+G10*Hx_zRe)+G01*(G10*Hz_xRe+G11*Hz_zRe)
	DHz_xRe = G10*(G00*Hx_xRe+G10*Hx_zRe)+G11*(G10*Hz_xRe+G11*Hz_zRe)
	DHy_zRe = G00*Hy_xRe+G01*Hy_zRe
	DHy_xRe = G10*Hy_xRe+G11*Hy_zRe
	DHx_zIm = G00*(G00*Hx_xIm+G10*Hx_zIm)+G01*(G10*Hz_xIm+G11*Hz_zIm)
	DHz_xIm = G10*(G00*Hx_xIm+G10*Hx_zIm)+G11*(G10*Hz_xIm+G11*Hz_zIm)
	DHy_zIm = G00*Hy_xIm+G01*Hy_zIm
	DHy_xIm = G10*Hy_xIm+G11*Hy_zIm
	#
	# Organize data to print
	dataRe, dataIm = [],[]
	datacurlRe, datacurlIm = [],[]
	for i in range(npnts):
		dataRe.append(np.zeros(HxRe[i].shape+(4,)))
		dataIm.append(np.zeros(HxIm[i].shape+(4,)))
		#
		dataRe[i][...,0] = uuRe[i] #Hx#
		dataRe[i][...,1] = vvRe[i] #Hy#
		dataRe[i][...,2] = wwRe[i] #Hz#
		#
		dataIm[i][...,0] = uuIm[i] #Hx#
		dataIm[i][...,1] = vvIm[i] #Hy#
		dataIm[i][...,2] = wwIm[i] #Hz#
		#
	XYZplot = XYZ

	dims = XYZ.shape[:-1] + (1,)
	xyz = XYZ.reshape(-1,3)

	########################### - Paraview - ############################

	for i in range(npnts):
		#
		HRe = dataRe[i][...,0:3].reshape(-1,3)
		HIm = dataIm[i][...,0:3].reshape(-1,3)
		#
		data = np.sqrt(dataRe[i][...,0:3]**2+dataIm[i][...,0:3]**2)
		HAbs = data[...,0:3].reshape(-1,3)
		#
		#####Hzz_x = (dataRe[i][...,0]+1j*dataIm[i][...,0])/np.cos(np.arctan(-1.25/7.4))#(dataRe[i][...,0]+1j*dataIm[i][...,0])*np.cos(np.arctan(-1.25/7.4))-(dataRe[i][...,2]+1j*dataIm[i][...,2])*np.sin(np.arctan(-1.25/7.4))#(dataRe[i][...,0]+1j*dataIm[i][...,0])/np.cos(np.arctan(-1.25/7.4))
		#####Hzz_z = (dataRe[i][...,2]+1j*dataIm[i][...,2])/np.sin(np.arctan(-1.25/7.4))#(dataRe[i][...,0]+1j*dataIm[i][...,0])*np.sin(np.arctan(-1.25/7.4))+(dataRe[i][...,2]+1j*dataIm[i][...,2])*np.cos(np.arctan(-1.25/7.4))#(dataRe[i][...,2]+1j*dataIm[i][...,2])/np.sin(np.arctan(-1.25/7.4))
		#####Hzz = np.sqrt(Hzz_x**2+Hzz_z**2)
		#
		Hzz_x  = (dataRe[i][...,0]+1j*dataIm[i][...,0])*np.cos(np.arctan(-1.25/7.4))-(dataRe[i][...,2]+1j*dataIm[i][...,2])*np.sin(np.arctan(-1.25/7.4))
		Hzz_z  = (dataRe[i][...,0]+1j*dataIm[i][...,0])*np.sin(np.arctan(-1.25/7.4))+(dataRe[i][...,2]+1j*dataIm[i][...,2])*np.cos(np.arctan(-1.25/7.4))
		Hzz_xm = np.sqrt(Hzz_x.real**2+Hzz_x.imag**2)
		Hzz_zm = np.sqrt(Hzz_z.real**2+Hzz_z.imag**2)
		Hzz    = np.sqrt(Hzz_x.real**2+Hzz_z.real**2)+1j*np.sqrt(Hzz_x.imag**2+Hzz_z.imag**2)
		#
		# vtk code
		vtk = VtkData(StructuredGrid(dims,xyz),
		              PointData(Vectors(HRe, 'Magnetic_field_Re'),
								Vectors(HIm, 'Magnetic_field_Im'),
								Vectors(HAbs,'Magnetic_field_abs'),
								#Scalars(Hzz_x.real, "Hzz_xRe"),
								#Scalars(Hzz_x.imag, "Hzz_xIm"),
								#Scalars(Hzz_xm, "Hzz_x"),
								#Scalars(Hzz_z.real, "Hzz_zRe"),
								#Scalars(Hzz_z.imag, "Hzz_zIm"),
								#Scalars(Hzz_zm, "Hzz_z"),
								#Scalars(Hzz.real, "Hzz_Re"),
								#Scalars(Hzz.imag, "Hzz_Im"),
					)
		)
		filename = "2.5D_EM_pos_"+str(i+sg*npnts)+".vtk"
		vtk.tofile(filename,format="ascii")
		print 'VTK file: '+Fore.GREEN +'created'+Fore.RESET
