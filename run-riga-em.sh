#!/bin/bash

# Other commands must follow all #SBATCH directives...

# =========================================== #
# Computing a 2D EM problem
# =========================================== #
## Discretization
# Dimension
d=2
# Number of knots in each direction
lkn=(200)
# Polynomial order
lp=(1)
# Number of subgrids
nsg=1
# Number of betas
nbt=0
# ============================================= #
# Parameters                                    # 
# ============================================= #
# mu
mu=4.0*pi*1E-7 
# epsilon
epsilon=8.85e-12       
# sigma
sigma=0.1            
# omega
omega=2.0*pi*2E6
# ============================================= #
# ============================================= #
# Main code                                     #
# ============================================= #
for kn in "${lkn[@]}"; do
  for p in "${lp[@]}"; do
    ## Nested dissections levels refined
    #  NDLr is the number of levels in each spatial dimension
    N=$kn^$d
    ND=$(echo "($kn+$p)^($d)" | bc)
    NDL=$(echo $ND | awk '{printf "%.i\n", $0}')
    NDLT=$(echo 'l('$NDL')/l(2)/'$d'' | bc -l)
    NDLr=${NDLT%.*}
    gp=$(echo "($p+1)" | bc)
    gp2=$(echo "($p+2)" | bc)
    k=$(echo "($p-1)" | bc)
    #
    echo "======================================================== "
    echo "  Computing Laplace problem with kn=$kn, p=$p and d=$d "
    echo "  Discretization "
    echo "  N=($kn+$p)^$d "
    echo "  p=$p "
    echo "  k=$p-1"
    echo "  No. of level refined: $NDLr x $d"
    echo "======================================================== "
    ## Solver
    echo "Solving problems"
    # rIGA
    for l in $(seq 0 1 0); do
      for sg in $(seq 1 1 $nsg); do
        for bt in $(seq 0 1 $nbt); do #$nbt
          echo "======================================================== "
          echo "--- Case with Level $l refined and beta = $bt"
          echo "======================================================== "
          echo "  - Create mesh files"
          python 2.5D_EM_meshgen.py $d $kn $p $l $sg 0
          echo " "
          echo "  - Solve the problem"
          ./2.5D_EM -nel $kn -l $l -sg $sg -beta $bt -mu $mu -ep $epsilon -om $omega -sm $sigma -Hx_iga_quadrature $gp2,$gp2 -Hy_iga_quadrature $gp2,$gp2 -Hz_iga_quadrature $gp2,$gp2 -Hx_iga_load geometry/grid_n$kn-p$p-c$k-l$l-sg$sg-Hx.dat -Hy_iga_load geometry/grid_n$kn-p$p-c$k-l$l-sg$sg-Hy.dat -Hz_iga_load geometry/grid_n$kn-p$p-c$k-l$l-sg$sg-Hz.dat -geo_iga_quadrature $gp2,$gp2 -geo_iga_load geometry/grid_n$kn-p$p-c$k-l$l-sg$sg-geo.dat -geo_lin_iga_load geometry/grid_n$kn-p$p-c$k-l$l-sg$sg-geo_lin.dat -log_view | tee log/2.5DEM-N$N-p$p-l$l-sg$sg.txt
          echo " "
          echo "  - Print Analytical and Numerical solution for single beta"
          python 2.5D_EM_solval.py $d $kn $p $l $bt 1 $sg 1 $mu $epsilon $sigma $omega
        done
        echo " "
        echo "  - Print vtk file"
        echo " "
        python 2.5D_EM_view.py $d $kn $p $l $bt 1 $sg 1
        echo "  - Print Analytical and Numerical solution by sum all betas"
        python 2.5D_EM_sol.py $d $kn $p $l $bt 1 $sg 1 $mu $epsilon $sigma $omega
      done
    done
  done
done
# ============================================= #
