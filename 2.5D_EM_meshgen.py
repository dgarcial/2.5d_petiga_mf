import sys
import numpy       as np
from   igakit.cad  import *
from   igakit.io   import *
from   igakit.plot import plt
from   colorama    import Fore, Back, Style 

# Knot generator
def generate_knots_trd(N,h,p):
    #
    U = [0]*p + list(range(N+1)) + [N]*p
    U = [1.0*(x)/int(N) for x in U]
    #
    return np.asarray(U, dtype='d')

def generate_knots_log(N,h,p):
    #
    sn = 4  # Number of elements in the interior of the mesh (interior mesh of sn*sn elements)
             # Note that the remaining elements are used to create the gradient of element size.
             # For now this is dirty, we can improve it by introduce sn by command terminal or 
             # do it automatically.
    #
    hm = 0.0
    #
    knots_in = []
    for i in range(int(0.5*sn)):
        hm = h*i
        knots_in.append(hm)
    #
    knots_ext = []
    for i in range(int(0.5*(N-sn))+2):
        hm = hm + h+h*np.log(i+1)
        knots_ext.append(hm)
    #
    knots_rht = knots_in[:-1]+knots_ext
    #
    knots_lft = knots_rht[::-1]
    knots_lft = [-1.0*x for x in knots_lft]
    knots = knots_lft[:-1]+knots_rht
    #
    U = [min(knots)]*p + knots + [max(knots)]*p
    #
    return np.asarray(U, dtype='d')

def generate_knots_test(N,h,p):
    #
    gm = 2
    sn = 0
    #
    knots1 = []
    for i in range(int(0.5*(N-2*sn))+1):
        knots1.append(h*i)
    #
    for i in range(sn):
        knots1.append(knots1[-1]+gm**(i+1)*h)
    #
    knots2 = knots1[::-1]
    knots2 = [-1.0*x for x in knots2]
    knots = knots2[:-1]+knots1
    #
    U = [min(knots)]*p + knots + [max(knots)]*p
    #
    return np.asarray(U, dtype='d')

# Grid generator
def generate_grid(x0,i,Nkn,p,base, breaks=()):
    assert base[0].dim == 1
    assert base[1].dim == 1
    base_x = base[0].copy()
    base_y = base[1].copy()
    px = base_x.degree[0]
    py = base_y.degree[0]
    for u in breaks:
        base_x.insert(0, u, p-i)
        base_y.insert(0, u, p-i)
    Ux = base_x.knots[0]+x0[0]#/(1.0*Nkn)*2.0*lg+x0[0]
    Uy = base_y.knots[0]+x0[1]#/(1.0*Nkn)*2.0*lg+x0[1]
    #
    #if d==2:
    grid = NURBS([Ux,Uy])
    return grid

# Print data
def print_grid(grid):
    plt.figure()
    plt.cplot(grid)
    plt.kplot(grid)
    #plt.plot(grid)
    #plt.kwire(grid, color=(0,0,0))
    #plt.kpoint(grid, color=(0,0,1))
    #plt.surface(grid, color=(0.7,0.7,0.7))
    plt.show()

# ============================================
## Main
#- Read data.
d      = int(sys.argv[1]) # dimension
Nkn    = int(sys.argv[2]) # Number on knots
p      = int(sys.argv[3]) # polynomial degree
nlevel = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,3,...
nsegts = int(sys.argv[5]) # number of segments e.g., 1,2,3,...
h = 0.03 # Elemental size
for x in range(1,nsegts+1):
    #- Data used to build the domain
    #- Building domain
    U  = generate_knots_trd(Nkn,h,p)
    Up = generate_knots_trd(Nkn,h,p+1)
    G  = generate_knots_log(Nkn,h,p)   # 
    Gp = generate_knots_log(Nkn,h,p+1) # Use _log for gradient meshes, use _test for regular meshes
    L  = generate_knots_log(Nkn,h,1)   # 
    # Div
    ## base_u = [NURBS([Up]),NURBS([U])]
    ## base_v = [NURBS([U]),NURBS([Up])]
    ## base_p = [NURBS([U]),NURBS([U])]
    # Curl X H1
    base_u = [NURBS([G]) ,NURBS([Gp])]
    base_v = [NURBS([Gp]),NURBS([Gp])]
    base_w = [NURBS([Gp]),NURBS([G])]
    base_g = [NURBS([L]) ,NURBS([L])]
    base_l = [NURBS([L]) ,NURBS([L])]
    #
    for level in range(nlevel+1):
        step = Nkn//2**level
        if step < 1: continue
        breaks_f = G[p:-p:step][1:-1]
        breaks_g = L[p:-p:step][1:-1]
        nrb_u = generate_grid([0,0],1,Nkn,p,base_u, breaks_f)
        nrb_v = generate_grid([0,0],1,Nkn,p,base_v, breaks_f)
        nrb_w = generate_grid([0,0],1,Nkn,p,base_w, breaks_f)
        #
        nrb_g = generate_grid([0,0],1,Nkn,1,base_g, breaks_g)
        nrb_l = generate_grid([0,0],1,Nkn,1,base_l, breaks_g)
        #
        if int(sys.argv[6])==1 and x==1:
            print "Geometry No.%i"%(level)
            print "grid-x No.%i"%(level)
            print_grid(nrb_u)
            print "Grid_y No.%i"%(level)
            print_grid(nrb_v)
            print "Grid_z No.%i"%(level)
            print_grid(nrb_w)
            print "Grid_g No.%i"%(level)
            print_grid(nrb_g)
            print "Grid_L No.%i"%(level)
            print_grid(nrb_l)
        #
        basename = "geometry/grid_n" + str(Nkn) + "-p" + str(p) + "-c" + str(p-1) + "-l" + str(level) + "-sg" + str(x) 
        PetIGA(scalar='complex').write(basename + "-Hx.dat",  nrb_u, control=False)
        print 'Mesh file of Hx (Hcurl): '+Fore.GREEN +'created'+Fore.RESET
        PetIGA(scalar='complex').write(basename + "-Hy.dat",  nrb_v, control=False)
        print 'Mesh file of Hy (H1)   : '+Fore.GREEN +'created'+Fore.RESET
        PetIGA(scalar='complex').write(basename + "-Hz.dat",  nrb_w, control=False)
        print 'Mesh file of Hz (Hcurl): '+Fore.GREEN +'created'+Fore.RESET
        PetIGA(scalar='complex').write(basename + "-geo.dat", nrb_g, nsd=2)
        print 'Mesh file of geometry mapping: '+Fore.GREEN +'created'+Fore.RESET
        PetIGA(scalar='complex').write(basename + "-geo_lin.dat", nrb_l, nsd=2)
        print 'Mesh file of linear geometry mapping: '+Fore.GREEN +'created'+Fore.RESET
        #
        #basename = "grid_n" + str(Nkn) + "-p" + str(p) + "-c" + str(p-1) + "-l" + str(level) 
        #PetIGA(indices='64bit').write(basename + "_u.dat", nrb_u, control=False)
        #PetIGA(indices='64bit').write(basename + "_v.dat", nrb_v, control=False)
        #PetIGA(indices='64bit').write(basename + "_p.dat", nrb_p, control=False)
        #PetIGA(indices='64bit').write(basename + "_geo.dat", nrb_p, control=False)