import glob,sys
import numpy         as np
import pylab         as plt
from   igakit.io     import PetIGA,VTK
from   igakit.nurbs  import NURBS
from   igakit.igalib import bsp,iga
from   scipy         import stats
from   scipy.special import kv
from   colorama      import Fore, Back, Style 

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def ispi(value):
  if value=='pi':
	return np.pi
  else:
    return 1.0

def convert2num(str):
  aux  = 1.0
  sstr = np.size(str.split("*"))
  for i in range(sstr):
  	line = str.split("*")[i]
  	if isfloat(line):
  		aux = aux*float(line)
  	else: 
  		aux = aux*ispi(line)
  return aux

# ============================================
# Equation to compute the exact value of Hzz
def HZZ(point,srcloc,srcmag,prm_user,case):
	#
	# Point
	x = point[0]
	z = point[1]
	# Source location
	x0 = srcloc[0]
	y0 = 0.0
	z0 = srcloc[1]
	# Source magnitude
	Mz = srcmag[2]
	# Parameter user
	Ly   = prm_user[0] # Length of the domain in the y direction
	nu   = prm_user[1] # Magnetic permeability
	ep   = prm_user[2] # Electric permeability
	sg   = prm_user[3] # Electric conductivity
	om   = prm_user[4] # Frequency
	beta = prm_user[5] # Fourier beta
	#
	Cn = 1.0*np.sqrt((2.0*np.pi*beta/Ly)**2+1j*nu*om*sg)
	Cn2= -(2.0*np.pi*beta/Ly)**2-1j*nu*om*sg
	Cn2= np.sqrt(-Cn2);
	R  = 1.0*np.sqrt((x-x0)**2+(z-z0)**2)
	K0=kv(0.0,Cn2*R)
	K1=kv(1.0,Cn2*R)
	K2=kv(2.0,Cn2*R);
	#
	if ((x==x0) and (z==z0)): case = 0 # Hack in order to not have problems when point = srcloc
	#
	if case==0:
		H  = Mz/(4.0*np.pi*Ly)*(-2*1j*nu*sg*om*kv(0.0,Cn*R)-
								(2*Cn*kv(1.0,Cn*R))/R+
			 					(2*(z-z0)**2*Cn*kv(1.0,Cn*R))/R**3+
			 					((z-z0)**2*(4*np.pi**2*beta**2+1j*Ly**2*nu*sg*om)*
			 						(kv(0.0,Cn*R)+kv(2.0,Cn*R)))/
			 							(Ly**2*(x**2-2*x*x0+x0**2+(z-z0)**2))
			 					)
	if case==1:
		H  = Mz/2.0/np.pi*1.0/Ly*(-1j*nu*om*sg*K0+1.0/2.0*(K0+K2)*(Cn2*(z-z0)/R)**2
			-K1*Cn2*(R**2-(z-z0)**2)/R**3)*np.exp(1j*2.0*np.pi*beta*y0/Ly)
	#
	ReH = H.real
	ImH = H.imag
	#
	return ReH,ImH

# ============================================
# Build exact solution
def exact(point,srcloc,srcmag,prm_user,method):
	H_real = []
	H_imag = []
	for i in range(np.size(point.T[0])):
		aux1,aux2 = HZZ(point[i],srcloc[i],srcmag[i],prm_user,method)
		H_real.append(aux1)
		H_imag.append(aux2)
	#
	return np.asarray(H_real),np.asarray(H_imag)

# ============================================
# Read FE point value
def readfiles(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	print "file "+namefile+" correctly read: "+Fore.GREEN+"yes"+Fore.RESET
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[12]));
		Hzzabs.append(float(line.split()[14]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)
#######################################################################################
# Read Fortran data for homogenous 3D case
def readfiles2(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	print "file "+namefile+" correctly read: "+Fore.GREEN+"yes"+Fore.RESET
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[11]));
		Hzzabs.append(float(line.split()[12]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)

# ============================================
# Euclidean norm
def norm(anasol,numsol):
	#
	x2 = np.zeros(np.size(anasol))
	for i in range(np.size(anasol)):
		x2[i] = (anasol[i]-numsol[i])**2
	en = np.sqrt(np.sum(x2))
	mx = np.sqrt(np.max(x2))
	mn = np.sqrt(np.min(x2))
	#
	return en,mx,mn

# ========================================================================#
#                                 Main Code                               #

########################### - Read from terminal - ########################

# Read data
d      = int(sys.argv[1]) # dimension
Nkn    = int(sys.argv[2]) # Number on knots
p      = int(sys.argv[3]) # polynomial degree
#
nlevel = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,...
nbetas = int(sys.argv[5]) # Fourier discretization of the y direction (number of betas) e.g., 0,1,2,...
npnts  = int(sys.argv[6]) # Number of receivers (per subdomain) e.g., 1,2,3,...
nsegt  = int(sys.argv[7]) # Number of subdomains e.g., 1,2,3,...
#
srcid  = int(sys.argv[8]) # Source case e.g., 1,2,3,...
#
#
mu      = convert2num(str(sys.argv[9]))  # mu
epsilon = convert2num(str(sys.argv[10])) # epsilon
sigma   = convert2num(str(sys.argv[11])) # sigma
omega   = convert2num(str(sys.argv[12])) # omega

########################### - Read from file - ############################

nrbG  = PetIGA(scalar='complex').read("geometry/grid_n"+str(Nkn)+"-p"+str(p)+"-c"+str(p-1)+"-l"+str(nlevel)+"-sg"+str(nsegt)+"-geo.dat")
print 'Mesh file of geometry  : '+Fore.GREEN +'correctly read'+Fore.RESET
Lx = nrbG.boundary(axis=0,side=1)([0])[0][0]-nrbG.boundary(axis=0,side=0)([0])[0][0]
Lz = nrbG.boundary(axis=1,side=1)([0])[0][1]-nrbG.boundary(axis=1,side=0)([0])[0][1]

######################## - Set additional param - #########################

prm_user = np.zeros(6)
prm_user[0] = max(Lx,Lz) # Ly
prm_user[1] = mu         # mu
prm_user[2] = epsilon    # epsilon
prm_user[3] = sigma      # sigma
prm_user[4] = omega      # omega
prm_user[5] = nbetas     # Beta

########################### - Read from file - ############################

HzzRe_ana = 0.0
HzzIm_ana = 0.0
HzzRe_num = 0.0
HzzIm_num = 0.0

dirf  = "Hzz/"
namef = "Hzz_"
extf  = ".txt"
for i in range(0,nbetas+1):
	middlef  = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(i)+"_sg"+str(nsegt)+"_Pos"+str(npnts)+"_Src"+str(srcid)
	filename = dirf+namef+middlef+extf 
	point_i,srcloc_i,srcmag_i,HzzRe_i,HzzIm_i,Hzzabs_i = readfiles(filename)
	#
	prm_user[5] = i # Beta
	Hzz_ana_real,Hzz_ana_imag = exact(point_i,srcloc_i,srcmag_i,prm_user,1) # Ali
	#
	if (i==0): c = 1.0
	if (i> 0): c = 2.0
	HzzRe_ana=HzzRe_ana+c*Hzz_ana_real
	HzzIm_ana=HzzIm_ana+c*Hzz_ana_imag
	HzzRe_num=HzzRe_num+c*HzzRe_i
	HzzIm_num=HzzIm_num+c*HzzIm_i

Hzz_ana = np.sqrt(HzzRe_ana**2+HzzIm_ana**2)
Hzz_num = np.sqrt(HzzRe_num**2+HzzIm_num**2)

#dirf     = "3D_homogeneous/"
#namef    = "Hzz_fortran_sigma_"
#middlef  = "0.1"
#extf     = ".txt"
#filename = dirf+namef+middlef+extf 
#point_3D,srcloc,srcmag,HzzRe_3D,HzzIm_3D,Hzz_3D = readfiles2(filename)

########################### - Plot comparison - ###########################

li  = 2
lf  = 12
Fli = 1
Flf = 101
fig, ax1 = plt.subplots(1,1)
ax1.plot(point_i.T[1][li:lf],HzzRe_num[li:lf],'bo',linewidth=2,label='Re(H)_rIGa') 
ax1.plot(point_i.T[1][li:lf],HzzRe_ana[li:lf],'b-',linewidth=2,label='Re(H)_Anal.')
#ax1.plot(point_3D.T[1][Fli,Flf],HzzRe_3D[Fli,Flf],'b--',linewidth=2,label='Re(H)_3D')
# 
ax1.tick_params('x', labelsize=15)
ax1.tick_params('y', labelsize=15)
plt.yscale('log')
plt.title("Full Real")
plt.legend()
# 
########################## - Save plot in file - ##########################

namef    = "Plot_Full_Hzz_Re_"
extf     = ".png"
filename = dirf+namef+middlef+extf 
plt.savefig(filename)
print "file "+filename+" correctly created: "+Fore.GREEN+"yes"+Fore.RESET

########################### - Plot comparison - ###########################

fig2, ax2 = plt.subplots(1,1)
ax2.plot(point_i.T[1][li:lf],-HzzIm_num[li:lf],'rv',linewidth=2,label='Im(H)_rIGa')
ax2.plot(point_i.T[1][li:lf],-HzzIm_ana[li:lf],'r-',linewidth=2,label='Im(H)_Anal.')
#ax2.plot(point_3D.T[1][Fli,Flf],HzzIm_3D[Fli,Flf],'r--',linewidth=2,label='Im(H)_3D')
ax2.tick_params('x', labelsize=15)
ax2.tick_params('y', labelsize=15)
plt.yscale('log')
plt.title("Full Imag")
plt.legend()

########################## - Save plot in file - ##########################

namef    = "Plot_Full_Hzz_Im_"
extf     = ".png"
filename = dirf+namef+middlef+extf 
plt.savefig(filename)
print "file "+filename+" correctly created: "+Fore.GREEN+"yes"+Fore.RESET

########################### - Plot comparison - ###########################
 
fig3, ax3 = plt.subplots(1,1)
ax3.plot(point_i.T[1][li:lf],Hzz_num[li:lf],'k*',linewidth=2,label='Abs(H)_rIGa')
ax3.plot(point_i.T[1][li:lf],Hzz_ana[li:lf],'k-',linewidth=2,label='Abs(H)_Anal.')
#ax3.plot(point_3D.T[1][Fli,Flf],Hzz_3D[Fli,Flf],'k--',linewidth=2,label='Abs(H)_3D')
ax3.tick_params('x', labelsize=15)
ax3.tick_params('y', labelsize=15)
plt.yscale('log')
plt.title("Full Abs")
plt.legend()

########################## - Save plot in file - ##########################

namef    = "Plot_Full_Hzz_Abs"
extf     = ".png"
filename = dirf+namef+middlef+extf 
plt.savefig(filename)
print "file "+filename+" correctly created: "+Fore.GREEN+"yes"+Fore.RESET

############################## - Error norm - #############################

norm,max,min = norm(Hzz_ana[li:lf],Hzz_num[li:lf])

print "Error norms: -norm = "+Fore.YELLOW+str(norm)+Fore.RESET
print "             -max  = "+Fore.YELLOW+str(max)+Fore.RESET
print "             -min  = "+Fore.YELLOW+str(min)+Fore.RESET