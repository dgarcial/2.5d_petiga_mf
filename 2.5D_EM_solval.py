import glob,sys
import numpy         as np
import pylab         as plt
from   igakit.io     import PetIGA,VTK
from   igakit.nurbs  import NURBS
from   igakit.igalib import bsp,iga
from   scipy         import stats
from   scipy.special import kv
from   colorama      import Fore, Back, Style 

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def ispi(value):
  if value=='pi':
	return np.pi
  else:
    return 1.0

def convert2num(str):
  aux  = 1.0
  sstr = np.size(str.split("*"))
  for i in range(sstr):
  	line = str.split("*")[i]
  	if isfloat(line):
  		aux = aux*float(line)
  	else: 
  		aux = aux*ispi(line)
  return aux

# ============================================
# Equation to compute the exact value of Hzz
def HZZ(point,srcloc,srcmag,prm_user,case):
	#
	# Point
	x = point[0]
	z = point[1]
	# Source location
	x0 = srcloc[0]
	y0 = 0.0
	z0 = srcloc[1]
	# Source magnitude
	Mz = srcmag[2]
	# Parameter user
	Ly   = prm_user[0] # Length of the domain in the y direction
	nu   = prm_user[1] # Magnetic permeability
	ep   = prm_user[2] # Electric permeability
	sg   = prm_user[3] # Electric conductivity
	om   = prm_user[4] # Frequency
	beta = prm_user[5] # Fourier beta
	#
	Cn  = 1.0*np.sqrt((2.0*np.pi*beta/Ly)**2+1j*nu*om*sg)
	Cn2 = -(2.0*np.pi*beta/Ly)**2-1j*nu*om*sg
	Cn2 = np.sqrt(-Cn2);
	R   = 1.0*np.sqrt((x-x0)**2+(z-z0)**2)
	K0  = kv(0.0,Cn2*R)
	K1  = kv(1.0,Cn2*R)
	K2  = kv(2.0,Cn2*R)
	#
	if ((x==x0) and (z==z0)): case = 0 # Hack in order to not have problems when point = srcloc
	#
	if case==0:
		H  = Mz/(4.0*np.pi*Ly)*(-2*1j*nu*sg*om*kv(0.0,Cn*R)-
								(2*Cn*kv(1.0,Cn*R))/R+
			 					(2*(z-z0)**2*Cn*kv(1.0,Cn*R))/R**3+
			 					((z-z0)**2*(4*np.pi**2*beta**2+1j*Ly**2*nu*sg*om)*
			 						(kv(0.0,Cn*R)+kv(2.0,Cn*R)))/
			 							(Ly**2*(x**2-2*x*x0+x0**2+(z-z0)**2))
			 					)
	if case==1:
		H  = Mz/2.0/np.pi*1.0/Ly*(-1j*nu*om*sg*K0+1.0/2.0*(K0+K2)*(1.0*Cn2*(z-z0)/R)**2
			-K1*Cn2*(R**2-(z-z0)**2)/R**3)*np.exp(1j*2.0*np.pi*beta*y0/Ly)
	#
	ReH = H.real
	ImH = H.imag
	Hm  = np.sqrt(ReH**2+ImH**2)
	#
	return H,Hm

# ============================================
# Read FE point value
def readfiles(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	print "file "+namefile+" correctly read: "+Fore.GREEN+"yes"+Fore.RESET
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[12]));
		Hzzabs.append(float(line.split()[14]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)

# ============================================
# Euclidean norm
def norm(anasol,numsol):
	#
	x2 = np.zeros(np.size(anasol))
	for i in range(np.size(anasol)):
		x2[i] = (anasol[i]-numsol[i])**2
	en = np.sqrt(np.sum(x2))
	mx = np.sqrt(np.max(x2))
	mn = np.sqrt(np.min(x2))
	#
	return en,mx,mn

# ========================================================================#
#                                 Main Code                               #

######################### - Read from terminal - ##########################

# Read data
d      = int(sys.argv[1]) # dimension
Nkn    = int(sys.argv[2]) # Number on knots
p      = int(sys.argv[3]) # polynomial degree
#
nlevel = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,...
nbetas = int(sys.argv[5]) # Fourier discretization of the y direction (number of betas) e.g., 0,1,2,...
npnts  = int(sys.argv[6]) # Number of receivers (per subdomain) e.g., 1,2,3,...
nsegt  = int(sys.argv[7]) # Number of subdomains e.g., 1,2,3,...
#
srcid  = int(sys.argv[8]) # Source case e.g., 1,2,3,...
#
#
mu      = convert2num(str(sys.argv[9]))  # mu
epsilon = convert2num(str(sys.argv[10])) # epsilon
sigma   = convert2num(str(sys.argv[11])) # sigma
omega   = convert2num(str(sys.argv[12])) # omega

########################### - Read from file - ############################

nrbG  = PetIGA(scalar='complex').read("geometry/grid_n"+str(Nkn)+"-p"+str(p)+"-c"+str(p-1)+"-l"+str(nlevel)+"-sg"+str(nsegt)+"-geo.dat")
print 'Mesh file of geometry  : '+Fore.GREEN +'correctly read'+Fore.RESET
Lx = nrbG.boundary(axis=0,side=1)([0])[0][0]-nrbG.boundary(axis=0,side=0)([0])[0][0]
Lz = nrbG.boundary(axis=1,side=1)([0])[0][1]-nrbG.boundary(axis=1,side=0)([0])[0][1]

########################### - Read from file - ############################

dirf     = "Hzz/"
namef    = "Hzz_"
middlef  = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(nbetas)+"_sg"+str(nsegt)+"_Pos"+str(npnts)+"_Src"+str(srcid)
extf     = ".txt"
filename = dirf+namef+middlef+extf 
point,srcloc,srcmag,HzzRe,HzzIm,Hzzabs = readfiles(filename)

######################## - Set additional param - #########################

prm_user = np.zeros(6)
prm_user[0] = max(Lx,Lz) # Ly
prm_user[1] = mu         # mu
prm_user[2] = epsilon    # epsilon
prm_user[3] = sigma      # sigma
prm_user[4] = omega      # omega
prm_user[5] = nbetas     # Beta

########################### - Build exact sol - ###########################

H  = [] # Old equation
Hm = [] # Old equation
H2  = [] # New equation (build by Ali)
Hm2 = [] # New equation (build by Ali)
for i in range(np.size(point.T[0])):
	aux1,aux2 = HZZ(point[i],srcloc[i],srcmag[i],prm_user,0)
	H.append(aux1)
	Hm.append(aux2)
	aux1,aux2 = HZZ(point[i],srcloc[i],srcmag[i],prm_user,1)
	H2.append(aux1)
	Hm2.append(aux2)

########################### - Plot comparison - ###########################

li = 2
lf =12
fig, ax1 = plt.subplots(1,1)
ax1.plot(point.T[1][li:lf],HzzRe[li:lf],'bo',linewidth=2,label='Re(H)_rIGa')
ax1.plot(point.T[1][li:lf],-HzzIm[li:lf],'rv',linewidth=2,label='Im(H)_rIGa')
ax1.plot(point.T[1][li:lf],Hzzabs[li:lf],'k*',linewidth=2,label='Abs(H)_rIGa')
# 
ax1.plot(point.T[1][li:lf],np.asarray(H).real[li:lf],'b-',linewidth=2,label='Re(H)_Anal.')
ax1.plot(point.T[1][li:lf],-np.asarray(H).imag[li:lf],'r-',linewidth=2,label='Re(H)_Anal.')
ax1.plot(point.T[1][li:lf],Hm2[li:lf],'k-',linewidth=2,label='Re(H)_Anal.')
# ax1.plot(point.T[1][li:lf],Hm[li:lf],'b-',linewidth=2)
ax1.tick_params('x', labelsize=15)
ax1.tick_params('y', colors='r',labelsize=15)
plt.yscale('log')
plt.legend()
# plt.title("Absolute")

########################## - Save plot in file - ##########################

# Print plot
namef    = "Plot_Hzz_"
extf     = ".png"
filename = dirf+namef+middlef+extf 
plt.savefig(filename)
print "file "+filename+" correctly created: "+Fore.GREEN+"yes"+Fore.RESET

############################## - Error norm - #############################

norm1,max1,min1 = norm(Hm[li:lf] ,Hzzabs[li:lf])
norm2,max2,min2 = norm(Hm2[li:lf],Hzzabs[li:lf])

print "Error norms: -norm(Dani) = "+Fore.YELLOW+str(norm1)+Fore.RESET
print "             -max(Dani)  = "+Fore.YELLOW+str(max1)+Fore.RESET
print "             -min(Dani)  = "+Fore.YELLOW+str(min1)+Fore.RESET
print "             -norm2(Ali) = "+Fore.YELLOW+str(norm2)+Fore.RESET
print "             -max(Ali)   = "+Fore.YELLOW+str(max2)+Fore.RESET
print "             -min(Ali)   = "+Fore.YELLOW+str(min2)+Fore.RESET