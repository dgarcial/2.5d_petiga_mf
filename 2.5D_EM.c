#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include "petiga.h"
#include "petigam.h"
#include "../src/petigaftn.h"

//----------------------------------------------------------------------------------------------------------------------------//
// User parameters
typedef struct {
  // Source data
  PetscReal   srcloc[3];
  PetscReal   srcmag[3];
  PetscInt    srcelem;
  // Properties (Improve tis part)
  PetscReal   mu;
  PetscReal   sigma;
  PetscReal   omega;
  PetscScalar sigma_bar[2][3];
  PetscScalar mu_bar;
  // Fourier discretization
  PetscInt    beta;
  PetscReal   Ly;
  // Optimization of the coeficients into the PDE
  PetscScalar Cnt_GH1_Hcurl[2][3];
  PetscScalar Cnt_Hcurl_Hcurl[2][3];
  // Solver type
  char solver_type[8];
} Params;

//----------------------------------------------------------------------------------------------------------------------------//
// Pi number
#define M_PI 3.14159265358979323846

//----------------------------------------------------------------------------------------------------------------------------//
// check if float
bool is_float(const char *s, PetscReal *dest) {
  if (s == NULL) {
    return PETSC_FALSE;
  }
  char *endptr;
  *dest = (PetscReal) strtod(s, &endptr);
  if (s == endptr) {
    return PETSC_FALSE; // no conversion;
  }
  // Look at trailing text
  while (isspace((unsigned char ) *endptr))
    endptr++;
  return *endptr == '\0';
}

//----------------------------------------------------------------------------------------------------------------------------//
// check if pi
void is_pi(const char *s, PetscReal *dest) {
  const char *s2 = "pi";
  PetscInt aux;
  if (s != NULL){
    aux = strcmp(s,s2);
    if (aux==0){*dest = PETSC_PI;}
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Converting
void cv_str2num(char string[],PetscReal *value)
{
  const char delimiters[] = "*";
  char * running = strdup(string);
  char * token;
  PetscReal aux=1.0;
  *value = 1.0;
  while( token != NULL ) { 
    token = strsep(&running, delimiters); 
    if (token == NULL){break;}
    is_float(token,&aux);
    is_pi(token,&aux);
    *value = *value * aux;
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Sigma property 
// This subroutine allow to set the values of sigma into the domain
void fsigma_loc(const PetscReal xyz[],PetscInt *loc)
{
  //PetscReal x  = xyz[0];
  //PetscReal z  = xyz[1];
  // This is used to set the property sigma (Identify in what zone we are when we are assembling the system)
  *loc=2;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Forcing (Source value)
// This subroutine set the source
void Forcing(const PetscReal xyz[],PetscScalar forcing[],void *ctx)
{
  Params *user  = (Params*)ctx;
  // Coordinates of the source
  //PetscReal x0 = user->srcloc[0];
  PetscReal y0 = user->srcloc[1];
  //PetscReal z0 = user->srcloc[2];
  // Source value
  forcing[0] = user->srcmag[0]*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
  forcing[1] = user->srcmag[1]*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
  forcing[2] = user->srcmag[2]*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Identify the element that contain the source
#undef  __FUNCT__
#define __FUNCT__ "Get_ele_src"
PetscErrorCode Get_ele_src(IGA iga, void *ctx)
{
  PetscErrorCode ierr;

  Params *user  = (Params*)ctx;

  IGAElement     element;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(iga,IGA_CLASSID,1);
  IGACheckSetUp(iga,1);

  /* Element loop */
  ierr = IGABeginElement(iga,&element);CHKERRQ(ierr);
  while (IGANextElement(iga,element)) {
    // Simple way to check if the point is into the element
    if (!element->geometry) SETERRQ1(PETSC_COMM_WORLD,1,"No geometry data. Geometry=%s\n",element->geometry ? "true" : "false");
    // point data
    PetscReal delta = 1e-5; // this delta is added to impose the source in a single node. I assume te source is always into the mesh far from the boundaries
    PetscReal     x = user->srcloc[0]+delta;
    //PetscReal     y = user->srcloc[1]+delta;
    PetscReal     z = user->srcloc[2]+delta;
    // geometry data
    PetscReal *X = element->geometryX;
    //
    PetscInt in=0;
    //
    PetscInt a = 0;
    PetscInt b = element->nen*element->nsd-element->nsd;
    if ( x <= X[a  ] ) in += 1;
    if ( z <= X[a+1] ) in += 1;
    if ( x >= X[b  ] ) in += 1;
    if ( z >= X[b+1] ) in += 1;
    //
    if ( in == 0 ) user->srcelem = element->index;
  }
  ierr = IGAEndElement(iga,&element);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

//----------------------------------------------------------------------------------------------------------------------------//
// Matrix formulation of the system problem
// This subroutine assemble the matrix K and vector f of the system Ku=f
#undef  __FUNCT__
#define __FUNCT__ "System"
PetscErrorCode System(IGAMPoint q,PetscScalar *K[],PetscScalar *F[],void *ctx)
{
  PetscErrorCode ierr;
  Params *user  = (Params*)ctx;
  
  /******** SYSTEM LAYOUT 2D ********
  
  In the paper (u,v,w) = (H_x,H_y,H_z)

             u     v     w
          |-----------------|
       u  |  0  |  1  |  2  |
          |-----------------|
       v  |  3  |  4  |  5  |
          |-----------------|
       w  |  6  |  7  |  8  |
          |-----------------|
       
  ***********************************/
  PetscInt  a,b; 
  PetscInt  dim,nen;
  PetscInt uu[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

  //----------------------------------------------//
  // Vectorial shape functions (Hcurl)
  PetscInt  nfld_curl;
  IGAPoint  *fld_curl    = NULL;
  PetscInt  *fldidx_curl = NULL;
  PetscReal **N_curl     = NULL;
  PetscReal **dNdX_curl  = NULL;

  dim         = q->ds_point[0]->dim;
  nfld_curl   = q->ds_point[0]->numfields;
  fldidx_curl = q->ds_point[0]->fields_idx;
  nen         = q->f_point[fldidx_curl[0]]->nen;

  ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
  ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
  ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&dNdX_curl);CHKERRQ(ierr);

  for(b=0;b<nfld_curl;b++){
    fld_curl[b]    =  q->f_point[fldidx_curl[b]];
    N_curl[b]      =  fld_curl[b]->basis[0];
    dNdX_curl[b]   =  fld_curl[b]->basis[1];
  }

  const IGAPoint x = q->g_point;
  
  const PetscReal (*DFinv) [dim] = (typeof(DFinv)) x->mapU[1];

  // Mapp functions  
  PetscReal Nx[nen];
  PetscReal Nz[nen];
  PetscReal dNxdz[nen];
  PetscReal dNzdx[nen];
  PetscReal (*dNdX_X)[dim] = (typeof(dNdX_X))dNdX_curl[0];
  PetscReal (*dNdX_Z)[dim] = (typeof(dNdX_Z))dNdX_curl[1];
  for (a=0; a<nen; a++) {
    Nx[a] = DFinv[0][0]*N_curl[0][a];
    Nz[a] = DFinv[1][1]*N_curl[1][a];
    dNxdz[a] = DFinv[0][0]*(dNdX_X[a][1]*DFinv[1][1]);
    dNzdx[a] = DFinv[1][1]*(dNdX_Z[a][0]*DFinv[0][0]);
  }
  
  //----------------------------------------------//
  // Scalar shape function (H1)
  
  IGAPoint  fld_H1;
  PetscInt  fldidx_H1;

  dim       = q->ds_point[1]->dim;
  nen       = q->ds_point[1]->nen;
  fldidx_H1 = q->ds_point[1]->fields_idx[0];

  fld_H1    = q->f_point[fldidx_H1];

  // Mapp functions
  PetscReal dNydx[nen];
  PetscReal dNydz[nen];
  PetscReal (*N_H1)         = (typeof(   N_H1)) fld_H1->basis[0];
  PetscReal (*dNdX_H1)[dim] = (typeof(dNdX_H1)) fld_H1->basis[1];
  for (a=0; a<nen; a++) {
    dNydx[a] = dNdX_H1[a][0]*DFinv[0][0];
    dNydz[a] = dNdX_H1[a][1]*DFinv[1][1];
  }

  //----------------------------------------------//
  // Point localization
  PetscReal xyz[2];
  IGAPointFormGeomMap(x,xyz);

  // Sigma value
  PetscInt loc=0;
  fsigma_loc(xyz,&loc);

  // Forcing
  PetscScalar f[3] = {0,0,0};
  Forcing(xyz,f,ctx);

  //----------------------------------------------//
  // Equation to solve
  {
    const IGAPoint *p = q->f_point;
    PetscScalar (*Juu)[p[0]->nen] = (typeof(Juu)) K[uu[0][0]];
    PetscScalar (*Juv)[p[0]->nen] = (typeof(Juv)) K[uu[1][0]];
    PetscScalar (*Juw)[p[0]->nen] = (typeof(Juw)) K[uu[2][0]];
    PetscScalar (*Jvu)[p[1]->nen] = (typeof(Jvu)) K[uu[0][1]];
    PetscScalar (*Jvv)[p[1]->nen] = (typeof(Jvv)) K[uu[1][1]];
    PetscScalar (*Jvw)[p[1]->nen] = (typeof(Jvw)) K[uu[2][1]];
    PetscScalar (*Jwu)[p[2]->nen] = (typeof(Jwu)) K[uu[0][2]];
    PetscScalar (*Jwv)[p[2]->nen] = (typeof(Jwv)) K[uu[1][2]];
    PetscScalar (*Jww)[p[2]->nen] = (typeof(Jww)) K[uu[2][2]];
    // 
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[0]->nen; b++){
        Juu[a][b] =  user->sigma_bar[0][loc]*(dNxdz[b]*dNxdz[a])+user->Cnt_Hcurl_Hcurl[1][loc]*Nx[b]*Nx[a];
        Juw[a][b] = -user->sigma_bar[0][loc]*(dNxdz[b]*dNzdx[a]);
        Jwu[a][b] = -user->sigma_bar[0][loc]*(dNzdx[b]*dNxdz[a]);
        Jww[a][b] =  user->sigma_bar[0][loc]*(dNzdx[b]*dNzdx[a])+user->Cnt_Hcurl_Hcurl[0][loc]*Nz[b]*Nz[a];
      }
      F[0][a] = -user->mu_bar*Nx[a]*f[0];
      F[2][a] = -user->mu_bar*Nz[a]*f[2];
    }
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[1]->nen; b++){
        Jvu[a][b] = -user->Cnt_GH1_Hcurl[1][loc]*(dNydx[b]*Nx[a]);
        Jvw[a][b] = -user->Cnt_GH1_Hcurl[0][loc]*(dNydz[b]*Nz[a]);
        Juv[b][a] =  user->Cnt_GH1_Hcurl[1][loc]*(Nx[a]*dNydx[b]);
        Jwv[b][a] =  user->Cnt_GH1_Hcurl[0][loc]*(Nz[a]*dNydz[b]);
      }
    }
    //
    for (a=0; a<p[1]->nen; a++) {
      for (b=0; b<p[1]->nen; b++){
        Jvv[a][b] = user->sigma_bar[1][loc]*(dNydx[b]*dNydx[a])+user->sigma_bar[0][loc]*(dNydz[b]*dNydz[a])+user->mu_bar*N_H1[b]*N_H1[a];
      }
      F[1][a] = -user->mu_bar*N_H1[a]*f[1];
    }
    return 0;
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Matrix formulation of the system problem
// This subroutine assemble the matrix K of the system Ku=f
#undef  __FUNCT__
#define __FUNCT__ "MatrixLHS"
PetscErrorCode MatrixLHS(IGAMPoint q,PetscScalar *K[],void *ctx)
{
  PetscErrorCode ierr;

  Params *user  = (Params*)ctx;

  /******** SYSTEM LAYOUT 2D ********
  
  In the paper (u,v,w) = (H_x,H_y,H_z)

             u     v     w
          |-----------------|
       u  |  0  |  1  |  2  |
          |-----------------|
       v  |  3  |  4  |  5  |
          |-----------------|
       w  |  6  |  7  |  8  |
          |-----------------|
       
  ***********************************/
  PetscInt  a,b;
  PetscInt  dim,nen;
  PetscInt uu[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Vectorial shape functions (Hcurl)
    PetscInt  nfld_curl;
    IGAPoint  *fld_curl    = NULL;
    PetscInt  *fldidx_curl = NULL;
    PetscReal **N_curl     = NULL;
    PetscReal **dNdX_curl  = NULL;
  
    dim         = q->ds_point[0]->dim;
    nfld_curl   = q->ds_point[0]->numfields;
    fldidx_curl = q->ds_point[0]->fields_idx;
    nen         = q->f_point[fldidx_curl[0]]->nen;
  
    ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&dNdX_curl);CHKERRQ(ierr);
  
    for(b=0;b<nfld_curl;b++){
      fld_curl[b]    =  q->f_point[fldidx_curl[b]];
      N_curl[b]      =  fld_curl[b]->basis[0];
      dNdX_curl[b]   =  fld_curl[b]->basis[1];
    }
    
    const IGAPoint x = q->g_point;
    const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];

    //----------------------------------------------//
    // Point localization
    PetscReal xyz[2];
    IGAPointFormGeomMap(x,xyz);
  
    // Sigma value
    PetscInt loc=0;
    fsigma_loc(xyz,&loc);
  
    //----------------------------------------------//
    //Mapp functions  
    PetscReal   Nx[nen],Nz[nen];
    PetscScalar Nx_h1_curl[nen],Nz_h1_curl[nen];
    PetscScalar Nx_curl[nen],Nz_curl[nen];
    PetscReal   dNxdz[nen],dNzdx[nen];
    PetscScalar dNxdz_curl[nen],dNzdx_curl[nen];
    PetscReal (*dNdX_X)[dim] = (typeof(dNdX_X))dNdX_curl[0];
    PetscReal (*dNdX_Z)[dim] = (typeof(dNdX_Z))dNdX_curl[1];
    for (a=0; a<nen; a++) {
      Nx[a]         = DFinv[0][0]*N_curl[0][a];
      Nz[a]         = DFinv[1][1]*N_curl[1][a];
      Nx_curl[a]    = user->Cnt_Hcurl_Hcurl[1][loc]*Nx[a];
      Nz_curl[a]    = user->Cnt_Hcurl_Hcurl[0][loc]*Nz[a];
      Nx_h1_curl[a] = user->Cnt_GH1_Hcurl[1][loc]*Nx[a];
      Nz_h1_curl[a] = user->Cnt_GH1_Hcurl[0][loc]*Nz[a];
      dNxdz[a]      = DFinv[0][0]*(dNdX_X[a][1]*DFinv[1][1]);
      dNzdx[a]      = DFinv[1][1]*(dNdX_Z[a][0]*DFinv[0][0]);
      dNxdz_curl[a] = user->sigma_bar[0][loc]*dNxdz[a];
      dNzdx_curl[a] = user->sigma_bar[0][loc]*dNzdx[a];
    }

    //----------------------------------------------//
    // Scalar shape functions (H1)
  
    IGAPoint  fld_H1;
    PetscInt  fldidx_H1;
    
    dim       = q->ds_point[1]->dim;
    nen       = q->ds_point[1]->nen;
    fldidx_H1 = q->ds_point[1]->fields_idx[0];
    
    fld_H1    = q->f_point[fldidx_H1];
    
    //Mapp functions
    PetscReal dNydx[nen],dNydz[nen];
    PetscScalar N_H1_h1[nen],dNydx_h1[nen],dNydz_h1[nen];
    PetscReal (*N_H1)         = (typeof(   N_H1)) fld_H1->basis[0];
    PetscReal (*dNdX_H1)[dim] = (typeof(dNdX_H1)) fld_H1->basis[1];
    for (a=0; a<nen; a++) {
      N_H1_h1[a]  = user->mu_bar*N_H1[a];
      dNydx[a]    = dNdX_H1[a][0]*DFinv[0][0];
      dNydz[a]    = dNdX_H1[a][1]*DFinv[1][1];
      dNydx_h1[a] = user->sigma_bar[1][loc]*dNydx[a];
      dNydz_h1[a] = user->sigma_bar[0][loc]*dNydz[a];
    }
  
    //----------------------------------------------//
    // Equation to solve
    const IGAPoint *p = q->f_point;
    PetscScalar (*Juu)[p[0]->nen] = (typeof(Juu)) K[uu[0][0]];
    PetscScalar (*Juv)[p[0]->nen] = (typeof(Juv)) K[uu[1][0]];
    PetscScalar (*Juw)[p[0]->nen] = (typeof(Juw)) K[uu[2][0]];
    PetscScalar (*Jvu)[p[1]->nen] = (typeof(Jvu)) K[uu[0][1]];
    PetscScalar (*Jvv)[p[1]->nen] = (typeof(Jvv)) K[uu[1][1]];
    PetscScalar (*Jvw)[p[1]->nen] = (typeof(Jvw)) K[uu[2][1]];
    PetscScalar (*Jwu)[p[2]->nen] = (typeof(Jwu)) K[uu[0][2]];
    PetscScalar (*Jwv)[p[2]->nen] = (typeof(Jwv)) K[uu[1][2]];
    PetscScalar (*Jww)[p[2]->nen] = (typeof(Jww)) K[uu[2][2]];
    //
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[0]->nen; b++){
        Juu[a][b] +=  (dNxdz[b]*dNxdz_curl[a])+Nx[b]*Nx_curl[a];
        Juw[a][b] += -(dNxdz[b]*dNzdx_curl[a]);
        Jwu[a][b] += -(dNzdx[b]*dNxdz_curl[a]);
        Jww[a][b] +=  (dNzdx[b]*dNzdx_curl[a])+Nz[b]*Nz_curl[a];
      }
      for (b=0; b<p[1]->nen; b++){
        Jvu[a][b] += -(dNydx[b]*Nx_h1_curl[a]);
        Jvw[a][b] += -(dNydz[b]*Nz_h1_curl[a]);
        Juv[b][a] +=  (Nx_h1_curl[a]*dNydx[b]);
        Jwv[b][a] +=  (Nz_h1_curl[a]*dNydz[b]);
      }
    }
    //
    for (a=0; a<p[1]->nen; a++) {
      for (b=0; b<p[1]->nen; b++){
        Jvv[a][b] += (dNydx[b]*dNydx_h1[a])+(dNydz[b]*dNydz_h1[a])+N_H1[b]*N_H1_h1[a];
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {

  }
  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Matrix formulation of the system problem
// This subroutine assemble the vector f of the system Ku=f
#undef  __FUNCT__
#define __FUNCT__ "VectorRHS"
PetscErrorCode VectorRHS(IGAMPoint q,PetscScalar *F[],void *ctx)
{
  PetscErrorCode ierr;

  Params *user  = (Params*)ctx;
     
  PetscInt  i,a,b;
  PetscInt  dim,nen;

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Check if we are in the source element
    IGAElement element; 
    element = q->parent->g_element;

//    //==========================================
//    PetscBool  In_element = PETSC_FALSE;
//    PetscReal  x = user->srcloc[0];
//    PetscReal  z = user->srcloc[2];
//    // geometry data
//    PetscReal *X = element->geometryX;
//    //
//    PetscInt in=0;
//    //
//    a = 0;
//    b = element->nen*element->nsd-element->nsd;
//    if ( x < X[a  ] ) in += 1;
//    if ( z < X[a+1] ) in += 1;
//    if ( x > X[b  ] ) in += 1;
//    if ( z > X[b+1] ) in += 1;
//    //
//    if ( in == 0 ) In_element = PETSC_TRUE;
//    //==========================================
//
//    if ( In_element )
    if ( user->srcelem == element->index )
    {
      //----------------------------------------------//
      // Vectorial shape functions (Hcurl)
      PetscInt  nfld_curl;
      IGAPoint  *fld_curl    = NULL;
      PetscInt  *fldidx_curl = NULL;
      PetscReal **N_curl     = NULL;
      
      dim         = q->ds_point[0]->dim;
      nfld_curl   = q->ds_point[0]->numfields;
      fldidx_curl = q->ds_point[0]->fields_idx;
      nen         = q->f_point[fldidx_curl[0]]->nen;
      
      ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
      ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
      
      for(b=0;b<nfld_curl;b++){
        fld_curl[b]    =  q->f_point[fldidx_curl[b]];
        N_curl[b]      =  fld_curl[b]->basis[0];
      }
      
      const IGAPoint x = q->g_point;
      const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];
  
      //Mapp functions  
      PetscReal Nx[nen],Nz[nen];
      for (a=0; a<nen; a++) {
        Nx[a] = DFinv[0][0]*N_curl[0][a];
        Nz[a] = DFinv[1][1]*N_curl[1][a];
      }
    
      //----------------------------------------------//
      // Scalar shape functions (H1)
    
      IGAPoint  fld_H1;
      PetscInt  fldidx_H1;
    
      dim       = q->ds_point[1]->dim;
      nen       = q->ds_point[1]->nen;
      fldidx_H1 = q->ds_point[1]->fields_idx[0];
    
      fld_H1    = q->f_point[fldidx_H1];
      
      PetscReal (*N_H1) = (typeof(   N_H1)) fld_H1->basis[0];
    
      //----------------------------------------------//
      // Point localization
      PetscReal xyz[2];
      IGAPointFormGeomMap(x,xyz);
      // Forcing
      PetscScalar f[3] = {0,0,0};
      Forcing(xyz,f,ctx);
      // Compute Area
      PetscReal Area = 0;
      for (i = 0; i < element->nqp; ++i){Area = Area + element->weight[i]*element->detJac[i];}

      //----------------------------------------------//
      // Equation to solve
      const IGAPoint *p = q->f_point;
      //
      for (a=0; a<p[0]->nen; a++) {
        F[0][a] += (-user->mu_bar*Nx[a]*f[0]/Area);
        F[2][a] += (-user->mu_bar*Nz[a]*f[2]/Area);
      }
      //
      for (a=0; a<p[1]->nen; a++) {
        F[1][a] += (-user->mu_bar*N_H1[a]*f[1]/Area);
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {

  }
  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// - Subroutines requires to compute puntal data 
// Set Basis
extern void IGA_Basis_BSpline (PetscInt i,PetscReal u,PetscInt p,PetscInt d,const PetscReal U[],PetscReal B[]);

//----------------------------------------------------------------------------------------------------------------------------//
// Get values/gradient/hessian/...
extern void IGA_GetValue(PetscInt nen,PetscInt dof,/*         */const PetscReal N[],const PetscScalar U[],PetscScalar u[]);

//----------------------------------------------------------------------------------------------------------------------------//
// Subroutine used to compute the span of a particular basis function
PetscInt IGA_FindSpan(PetscInt n,PetscInt p,PetscReal u, const PetscReal U[])
{
  PetscInt low,high,span;
  if (PetscUnlikely(u <= U[p]))   return p;
  if (PetscUnlikely(u >= U[n+1])) return n;
  low  = p;
  high = n+1;
  span = (high+low)/2;
  while (u < U[span] || u >= U[span+1]) {
    if (u < U[span]) {
      high = span;
    } else {
      low = span;
    }
    span = (high+low)/2;
  }
  return span;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Subroutine used to compute point values
#undef  __FUNCT__
#define __FUNCT__ "GetRValue"CURL
PetscErrorCode GetRValue(IGAM iga, PetscInt fld, Vec x, PetscReal point[], PetscScalar u[])
{ 
  // NOTE: point has the dimension of 2 and contains the values x,z.
  //----------------------------------------------//
  // Variables
  PetscInt i,j,k,aux,aux2;
  PetscBool offprocess[fld+1];
  PetscBool collective = PETSC_FALSE;
  // Vectorial data
  Vec lvec[fld];
  Vec gvec[fld];
  
  const PetscScalar *arrayA[fld];
  // Geometry data
  const PetscReal   *arrayW; 
  const PetscReal   *arrayX; 
  // Dimension of the problem
  PetscInt     dim;
  PetscInt     nsd;
  dim = iga->g_iga->dim; 
  nsd = iga->g_iga->geometry ? iga->g_iga->geometry : iga->g_iga->dim;
  // Polynomial order
  PetscInt     order[fld+1];
  // Degrees of freedom
  PetscInt     dof=1; // We are use one dof per space
  // Knot vector and polynomial order
  PetscReal   *U[fld+1][nsd];
  PetscInt     p[fld+1][nsd];
  PetscInt     n[fld+1][nsd];
  PetscInt     s[fld+1][nsd];
  PetscInt     w[fld+1][nsd];
  // Number of local basis functions
  PetscInt     nen[fld+1];
  // Mappings
  PetscInt    *map[fld+1];
  // Local dof nodal values
  PetscScalar *A[fld];
  // Local rational 
  PetscReal   *W;
  // Local geometry data
  PetscReal   *X;
  PetscInt     ndiff = 5;
  PetscReal   *gbasis[1][ndiff];
  PetscReal   *mapU[ndiff];
  PetscReal   *mapX[ndiff];
  PetscReal   *detX;
  // FE 
  PetscInt     ID[fld+1][3];
  PetscReal   *BD[fld+1][nsd];
  PetscReal   *fbasis[fld][ndiff];

  PetscErrorCode ierr;

  //----------------------------------------------//
  // Extract the FE solution value per the fields (Hx,Hy and Hz)
  Vec *vecs;
  ierr = IGAMUnpackVec(iga,x,&vecs);CHKERRQ(ierr);
  for (i=0; i<fld; i++) {
    ierr = VecDuplicate(vecs[i],&gvec[i]);CHKERRQ(ierr);
    ierr = IGAGetOrder(iga->f_iga[i],&order[i]);CHKERRQ(ierr);
    ierr = VecCopy(vecs[i],gvec[i]);CHKERRQ(ierr);
    // This lines gives the array and local vec (in case we use parallel solution)
    ierr = IGAGetLocalVecArray(iga->f_iga[i],gvec[i],&lvec[i],&arrayA[i]);CHKERRQ(ierr); 
  }
  ierr = IGAMRepackVec(iga,x,&vecs);CHKERRQ(ierr);
  
  //----------------------------------------------//
  // Fields BD allocation
  for (i=0; i<fld; i++) {
    // Initialize number of local basis functions
    nen[i]=1;
    // Loop on dimensions
    for (j=0; j<nsd; j++) {
      p[i][j] = iga->f_iga[i]->axis[j]->p;     // Polynomial order
      U[i][j] = iga->f_iga[i]->axis[j]->U;     // Knot vector
      n[i][j] = iga->f_iga[i]->geom_sizes[j];  // Size of the 1D geom
      s[i][j] = iga->f_iga[i]->geom_gstart[j]; // Global start
      w[i][j] = iga->f_iga[i]->geom_gwidth[j]; // Global width
      nen[i] *= (p[i][j]+1);                   // Number of local basis functions
      ierr = PetscMalloc1((size_t)((p[i][j]+1)*5),&BD[i][j]);CHKERRQ(ierr);
    }
    // Allocate memory
    ierr = PetscCalloc1(nen[i]/**/,&map[i]/*    */);CHKERRQ(ierr);
    ierr = PetscCalloc1(nen[i]*dof,&A[i]/*      */);CHKERRQ(ierr);
    for (j = 0; j < ndiff; j++){
      aux  = pow(dim,j);
      ierr = PetscCalloc1(nen[i]*aux,&fbasis[i][j]);CHKERRQ(ierr);
    }
  }

  //----------------------------------------------//
  // Geometry BD allocation
  // Initialize number of local basis functions
  nen[fld]=1;
  // Loop on dimensions
  for (j=0; j<nsd; j++) {
    p[fld][j] = iga->g_iga->axis[j]->p;     // Polynomial order
    U[fld][j] = iga->g_iga->axis[j]->U;     // Knot vector
    n[fld][j] = iga->g_iga->geom_sizes[j];  // Size of the 1D geom
    s[fld][j] = iga->g_iga->geom_gstart[j]; // Global start
    w[fld][j] = iga->g_iga->geom_gwidth[j]; // Global width
    nen[fld] *= (p[fld][j]+1);              // Number of local basis functions
    ierr = PetscMalloc1((size_t)((p[fld][j]+1)*5),&BD[fld][j]);CHKERRQ(ierr);
  }
  // Allocate memory
  ierr = PetscCalloc1(nen[fld]/**/,&map[fld]);CHKERRQ(ierr);
  ierr = PetscCalloc1(nen[fld]/**/,&W/*   */);CHKERRQ(ierr);
  ierr = PetscCalloc1(nen[fld]*nsd,&X/*   */);CHKERRQ(ierr);
  for (j=0; j<ndiff; j++) 
  {
    aux  = pow(dim,j);
    aux2 = pow(nsd,j);
    ierr = PetscCalloc1(nen[fld]*aux,&gbasis[0][j]);CHKERRQ(ierr);
    //ierr = PetscMemzero(gbasis[0][j],sizeof(gbasis[0][j]));CHKERRQ(ierr);
    ierr = PetscCalloc1(dim*aux2/**/,&mapU[j]  );CHKERRQ(ierr);
    //ierr = PetscMemzero(mapU[j],sizeof(mapU[j]));CHKERRQ(ierr);
    ierr = PetscCalloc1(nsd*aux/* */,&mapX[j]  );CHKERRQ(ierr);
    //ierr = PetscMemzero(mapX[j],sizeof(mapX[j]));CHKERRQ(ierr);
  }
  ierr = PetscCalloc1(1,&detX);CHKERRQ(ierr);
  ierr = IGAGetOrder(iga->g_iga,&order[fld]);CHKERRQ(ierr);
  //
  detX[0] = 1.0;
  for (j=0; j<dim; j++) mapU[1][j*(nsd+1)] = 1.0;
  for (j=0; j<nsd; j++) mapX[1][j*(dim+1)] = 1.0;

  //----------------------------------------------//
  // Span index (obtain the set of basis functions indexes required to compute the point value)
  for (i=0; i<fld+1; i++){
    for (j=0; j<nsd; j++){ID[i][j] = IGA_FindSpan(n[i][j]-1,p[i][j],point[j],U[i][j]);}
    offprocess[i] = PETSC_FALSE;
    for (j=0; j<nsd; j++) {
      PetscInt first = ID[i][j]-p[i][j], last = first + p[i][j];
      PetscInt start = s[i][j], end = s[i][j] + w[i][j];
      if (first < start || last >= end) offprocess[i] = PETSC_TRUE;
    }
    if (PetscUnlikely(offprocess[i] && !collective)) PetscFunctionReturn(0);
  }

  //----------------------------------------------//
  // Span closure (Field)
  for (i=0; i<fld; i++) {
    if (PetscLikely(!offprocess[i])) {
      PetscInt ia, inen, ioffset, istart;
      PetscInt ja, jnen, joffset, jstart;
      //PetscInt ka, knen, koffset, kstart;
      PetscInt pos, jstride;//, kstride;
      PetscInt iA, jA;//, kA;
      //
      PetscInt *IDf = ID[i], *pf = p[i], *sf = s[i], *wf = w[i];
      inen = pf[0]+1; ioffset = IDf[0]-pf[0]; istart = sf[0];
      jnen = pf[1]+1; joffset = IDf[1]-pf[1]; jstart = sf[1];
      //knen = pf[2]+1; koffset = IDf[2]-pf[2]; kstart = sf[2];
      pos = 0, jstride = wf[0];//, kstride = wf[0]*wf[1];
      // Build map to go from global to local basis functions
    ierr = PetscFree(W/*   */);CHKERRQ(ierr);
    //for (ka=0; ka<knen; ka++) {
        for (ja=0; ja<jnen; ja++) {
          for (ia=0; ia<inen; ia++) {
            iA = (ioffset + ia) - istart;
            jA = (joffset + ja) - jstart;
            //kA = (koffset + ka) - kstart;
            map[i][pos++] = iA + jA*jstride;// + kA*kstride;
          }
        }
      //}
      // Obtain the nodal value (dof values) of the local basis fucntions required to compute the point value
      if (arrayA[i]){for (k=0; k<nen[i]; k++){for (j=0; j<dof; j++){A[i][j + k*dof] = arrayA[i][j + map[i][k]*dof];}}}
    }

  //----------------------------------------------//
  // Compute 1D basis functions
    void (*ComputeBasis)(PetscInt,PetscReal,PetscInt,PetscInt,const PetscReal[],PetscReal[]) = NULL;
    for (j=0; j<dim; j++) {
      ComputeBasis = IGA_Basis_BSpline;
      ComputeBasis(ID[i][j],point[j],p[i][j],order[i],U[i][j],BD[i][j]);
    }    

    //----------------------------------------------//
    // Tensor product of 1D basis functions to build a 2D basis function
      IGA_BasisFuns_2D(order[i],1,p[i][0]+1,BD[i][0],
                                1,p[i][1]+1,BD[i][1],
                                fbasis[i][0],fbasis[i][1],fbasis[i][2],fbasis[i][3],fbasis[i][4]);
  }

  //----------------------------------------------//
  // Span closure (Geometry)
  {
    if (PetscLikely(!offprocess[i])) {
      PetscInt ia, inen, ioffset, istart;
      PetscInt ja, jnen, joffset, jstart;
      //PetscInt ka, knen, koffset, kstart;
      PetscInt pos, jstride;//, kstride;
      PetscInt iA, jA;//, kA;
      //
      PetscInt *IDg = ID[fld], *pg = p[fld], *sg = s[fld], *wg = w[fld];
      inen = pg[0]+1; ioffset = IDg[0]-pg[0]; istart = sg[0];
      jnen = pg[1]+1; joffset = IDg[1]-pg[1]; jstart = sg[1];
      //knen = pg[2]+1; koffset = IDg[2]-pg[2]; kstart = sg[2];
      pos = 0, jstride = wg[0];//, kstride = wg[0]*wg[1];
      // Build map to go from global to local basis functions
      //for (ka=0; ka<knen; ka++) {
        for (ja=0; ja<jnen; ja++) {
          for (ia=0; ia<inen; ia++) {
            iA = (ioffset + ia) - istart;
            jA = (joffset + ja) - jstart;
            //kA = (koffset + ka) - kstart;
            map[fld][pos++] = iA + jA*jstride;// + kA*kstride;
          }
        }
      //}
      // If rational
      if (iga->g_iga->rational && iga->g_iga->rationalW){
        arrayW = iga->g_iga->rationalW;
        for (j=0; j<nen[fld]; j++){W[j] = arrayW[map[fld][j]];}
      }
      // If geometry
      if (iga->g_iga->geometry && iga->g_iga->geometryX){
        arrayX = iga->g_iga->geometryX;
        for (k=0; k<nen[fld]; k++){for (j=0; j<nsd; j++){X[j + k*nsd] = arrayX[j + map[fld][k]*nsd];}}
      }
    }


    //----------------------------------------------//
    // Compute 1D basis functions
    void (*ComputeBasis)(PetscInt,PetscReal,PetscInt,PetscInt,const PetscReal[],PetscReal[]) = NULL;
    for (j=0; j<dim; j++) {
      ComputeBasis = IGA_Basis_BSpline;
      ComputeBasis(ID[fld][j],point[j],p[fld][j],order[fld],U[fld][j],BD[fld][j]);
    }    

    //----------------------------------------------//
    // Tensor product of 1D basis functions to build a 2D basis function
    IGA_BasisFuns_2D(order[fld],1,p[fld][0]+1,BD[fld][0],
                                1,p[fld][1]+1,BD[fld][1],
                                gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4]);

    //----------------------------------------------//U
    // Rationalize basis functions
    if (iga->g_iga->rational && iga->g_iga->rationalW){
      IGA_Rationalize_2D(order[fld],1,nen[fld],W,gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4]);
    }
    
    //----------------------------------------------//
    // Geometry mapping
    for (i=0; i<dim; i++){mapU[0][i] = point[i];mapX[0][i] = point[i];}
    if (iga->g_iga->geometry && iga->g_iga->geometryX){
      if (PetscLikely(dim == nsd))
        IGA_GeometryMap_2D(order[fld],1,nen[fld],X,gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4],
                                                   mapX[0]  ,mapX[1]  ,mapX[2]  ,mapX[3]  ,mapX[4]  );
      else
        IGA_GeometryMap(order[fld],dim,nsd,1,nen[fld],X,gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4],
                                                        mapX[0]  ,mapX[1]  ,mapX[2]  ,mapX[3]  ,mapX[4]  );
    }

    //----------------------------------------------//
    // Geometry inverse mapping
    if (arrayX && PetscLikely(dim == nsd)) {
      IGA_InverseMap_2D(order[fld],1,mapX[1],mapX[2],mapX[3],mapX[4],detX,
                                     mapU[1],mapU[2],mapU[3],mapU[4]);
    }
  }

  //----------------------------------------------//
  // Shape functions
  PetscInt    nfld_curl = 2;    // Number of Hcurl fields (In this case is 2)
  PetscReal **N_curl    = NULL;
  // Allocate memory
  ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
  // Build curl basis functions
  N_curl[0] = fbasis[0][0];
  N_curl[1] = fbasis[2][0];
  // Build Mapping
  const PetscReal (*DFinv) [dim] = (typeof(DFinv))  mapU[1];
  // Shape functions
  PetscReal Nx[nen[0]];
  PetscReal Nz[nen[2]];
  for (i=0; i<nen[0]; i++) {
    Nx[i] = DFinv[0][0]*N_curl[0][i]+DFinv[1][0]*N_curl[1][i];
    Nz[i] = DFinv[0][1]*N_curl[0][i]+DFinv[1][1]*N_curl[1][i];
  }

  //----------------------------------------------//
  // Compute puntual value
  IGA_GetValue(nen[0],dof,Nx,A[0],&u[0]);
  IGA_GetValue(nen[0],dof,Nz,A[2],&u[1]);

  //----------------------------------------------//
  // Free memory
  for (i=0; i<fld; i++) {
    lvec[i]   = NULL;
    arrayA[i] = NULL;
    A[i]      = NULL;
    for (j = 0; j < nsd; ++j){
      U[i][j] = NULL;
    }
    for (j = 0; j < ndiff; ++j){
      if (fbasis[i][j]!=NULL)
      {
        ierr = PetscFree(fbasis[i][j]);CHKERRQ(ierr);
        fbasis[i][j] = NULL;
      }
    }
    ierr = PetscFree(map[i]);CHKERRQ(ierr);
    map[i]=NULL;
  }  
  arrayW = NULL;
  arrayX = NULL;
  for (i=0; i<fld; i++) {
    for (j = 0; j < nsd; ++j){
      ierr = PetscFree(BD[i][j]);CHKERRQ(ierr);
      BD[i][j] = NULL;
    }
  }
  for (j = 0; j < nsd; ++j){
    U[i][j] = NULL;
  }
  ierr = PetscFree(map[fld]);CHKERRQ(ierr);
  map[fld]=NULL;
  ierr = PetscFree(W/*   */);CHKERRQ(ierr);
  W = NULL;
  ierr = PetscFree(X/*   */);CHKERRQ(ierr);
  X = NULL;
  for (j = 0; j < ndiff; ++j){
    if (gbasis[0][j]!=NULL)
    {
      ierr = PetscFree(gbasis[0][j]);CHKERRQ(ierr);
      gbasis[0][j] = NULL;
    }
    if (mapU[j]!=NULL)
    {
      ierr = PetscFree(mapU[j]);CHKERRQ(ierr);
      mapU[j] = NULL;
    }
    if (mapX[j]!=NULL)
    {
      ierr = PetscFree(mapX[j]);CHKERRQ(ierr);
      mapX[j] = NULL;
    }
  }
  ierr = PetscFree(detX);CHKERRQ(ierr);
  detX = NULL;

  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Main code
#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[]) {
  
  IGAM iga;
  PetscInt i,j,nel,l,sg;
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc,&argv,0,0);CHKERRQ(ierr);

  ierr =  PetscMemorySetGetMaximumUsage();CHKERRQ(ierr);

  //----------------------------------------------//
  // User parameters
  Params user;
  // Set Number of fields
  PetscInt fld = 3; // H(curl) x H1
  // Set dimension of the problem
  PetscInt dim = 2;
  // Initialize and set beta parameter of the Fourier discretization
  user.beta   = 0;
  ierr = PetscOptionsGetInt(NULL,NULL,"-beta",&user.beta,NULL);CHKERRQ(ierr);
  // Initialize length of the domain in the y axis direction
  user.Ly     = 0;
  // Set the number of elements
  ierr = PetscOptionsGetInt(NULL,NULL,"-nel",&nel,NULL);CHKERRQ(ierr);
  // Set the level od partitioning using C^0 separators
  ierr = PetscOptionsGetInt(NULL,NULL,"-l",&l,NULL);CHKERRQ(ierr);
  // Set the Segment of the domain where we are located (This is used when we consider snapshots)
  ierr = PetscOptionsGetInt(NULL,NULL,"-sg",&sg,NULL);CHKERRQ(ierr);
  // Set the Magnetic permeability
  PetscReal mu=0; // nu      -> magnetic permeability
  char string1[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-mu",string1,sizeof(string1),NULL);CHKERRQ(ierr);
  cv_str2num(string1,&mu);
  // Set the Electric permeability
  PetscReal epsilon=0; // epsilon -> electric permeability
  char string2[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-ep",string2,sizeof(string2),NULL);CHKERRQ(ierr);
  cv_str2num(string2,&epsilon);
  // set the frequency
  PetscReal omega=0; // omega   -> frequency
  char string3[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-om",string3,sizeof(string3),NULL);CHKERRQ(ierr);
  cv_str2num(string3,&omega);
  // Set the resistivity
  PetscReal sigma=0; // sigma   -> resistivity
  char string4[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-sm",string4,sizeof(string4),NULL);CHKERRQ(ierr);
  cv_str2num(string4,&sigma);
  //
  PetscReal sigmas[3] = {0.1,0.1,0.1};
  // Set the Direct solver package
  strcpy(user.solver_type,"MUMPS");

  //----------------------------------------------//
  // Paramaters to compute the exact solution (Homogeneous case)
  user.mu    = mu;
  user.omega = omega;
  user.sigma = sigmas[0];

  //----------------------------------------------//
  // This section identify the element that contain the source
  // The cost of this section is linearly dependent of the number of elements
  IGA geom_lin;
  ierr = IGACreate(PETSC_COMM_WORLD,&geom_lin);CHKERRQ(ierr);
  ierr = IGASetDof(geom_lin,1);CHKERRQ(ierr);
  ierr = IGASetDim(geom_lin,dim);CHKERRQ(ierr);  
  ierr = IGASetOptionsPrefix(geom_lin,"geo_lin_");CHKERRQ(ierr);
  ierr = IGASetFromOptions(geom_lin);CHKERRQ(ierr);
  ierr = IGASetUp(geom_lin);CHKERRQ(ierr);

  // Set length of the domain in the y axis direction assuming we work on a cube
  PetscReal Ui,Uf;
  ierr = IGAAxisGetLimits(geom_lin->axis[0],&Ui,&Uf);CHKERRQ(ierr);
  user.Ly = Uf-Ui;

  //----------------------------------------------//
  // Set the parameters used to build the matrix system of the PDE
  // NOTE: I will improve this part converting it in elemental values, but it is future work
  for (i=0; i<3; i++)
  {
    PetscReal sigma_h = sigmas[i];// sigma   -> electric conductivity
    PetscReal sigma_v = sigmas[i];// sigma   -> electric conductivity
    user.mu_bar = (PETSC_i*mu*omega);
    user.sigma_bar[0][i] = (1.0/(sigma_h+PETSC_i*epsilon*omega));
    user.sigma_bar[1][i] = (1.0/(sigma_v+PETSC_i*epsilon*omega));
    //
    PetscReal Cnt_Fmode        = user.beta*2.0*PETSC_PI/user.Ly;
    user.Cnt_GH1_Hcurl[0][i]   = user.sigma_bar[0][i]*Cnt_Fmode*PETSC_i;
    user.Cnt_GH1_Hcurl[1][i]   = user.sigma_bar[1][i]*Cnt_Fmode*PETSC_i;
    user.Cnt_Hcurl_Hcurl[0][i] = user.sigma_bar[0][i]*pow(Cnt_Fmode,2)+user.mu_bar;
    user.Cnt_Hcurl_Hcurl[1][i] = user.sigma_bar[1][i]*pow(Cnt_Fmode,2)+user.mu_bar;
  }

  //----------------------------------------------//
  // In this section we create the fields required to perform a FE analysis
  IGA geom;
  ierr = IGACreate(PETSC_COMM_WORLD,&geom);CHKERRQ(ierr);
  ierr = IGASetDof(geom,1);CHKERRQ(ierr);
  ierr = IGASetDim(geom,dim);CHKERRQ(ierr);
  ierr = IGASetOptionsPrefix(geom,"geo_");CHKERRQ(ierr);
  IGA field[fld+1]; const char *prefix[] = {"Hx_","Hy_","Hz_"};
  for (i=0; i<fld; i++) {
    ierr = IGACreate(PETSC_COMM_WORLD,&field[i]);CHKERRQ(ierr);
    ierr = IGASetDof(field[i],1);CHKERRQ(ierr);
    ierr = IGASetDim(field[i],dim);CHKERRQ(ierr);
    ierr = IGASetOptionsPrefix(field[i],prefix[i]);CHKERRQ(ierr);
  }
  // Creation IGAM, object that contain the IGA fields
  ierr = IGAMCreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
  ierr = IGAMCompose(iga,geom,fld,field);CHKERRQ(ierr);
  ierr = IGAMSetFromOptions(iga);CHKERRQ(ierr);
  ierr = IGAMSetUp(iga);CHKERRQ(ierr);
  // Set of the Dirichlet boundary conditions
  PetscInt fldbnd[2] = {0,2}; 
  for (i=0; i<dim; i++) {
    PetscInt axis=i,side;
    for (side=0; side<2; side++) {
      ierr = IGAMSetBoundaryValue(iga,axis,side,fldbnd[1-i],0.0);CHKERRQ(ierr);
      ierr = IGAMSetBoundaryForm (iga,axis,side,PETSC_TRUE);CHKERRQ(ierr);
    }
  }     

  //----------------------------------------------//
  // Set the number of discrete spaces (In this case 2 1) Hcurl 2) H1)
  iga->numds = 2;
  // H(curl) space
  PetscInt curl_flds[] = {0,2};
  ierr = IGAMSetDS(iga,0,CURL_CONFORMING,2,curl_flds);CHKERRQ(ierr);
  // H1 space
  PetscInt H1_flds[] = {1};
  ierr = IGAMSetDS(iga,1,H1_CONFORMING,1,H1_flds);CHKERRQ(ierr);
  //Must be called after IGAMSetUp()
  ierr = IGAMDSElementInit(iga);CHKERRQ(ierr);
  // Order degree
  PetscInt p,ord=100;
  IGAAxis  axis;
  ierr = IGAGetAxis(field[1],0,&axis);CHKERRQ(ierr);
  ierr = IGAAxisGetDegree(axis,&p);CHKERRQ(ierr);
  ord  = PetscMin(ord,p);

  //----------------------------------------------//
  // System
  //iga->form->ops->System = System;
  //iga->form->ops->SysCtx = &user;
  // Matrix
  iga->form->ops->Matrix = MatrixLHS;
  iga->form->ops->MatCtx = &user;
  // Vector
  iga->form->ops->Vector = VectorRHS;
  iga->form->ops->VecCtx = &user;

  //----------------------------------------------//
  //Build the helmholtz system
  Mat A;
  Vec x,b;
  ierr = IGAMCreateMat(iga,&A);CHKERRQ(ierr);
  ierr = IGAMCreateVec(iga,&x);CHKERRQ(ierr);
  ierr = IGAMCreateVec(iga,&b);CHKERRQ(ierr);
  //ierr = IGAMSetFormSystem(iga_ma,SystemHel,NULL);CHKERRQ(ierr);
  //ierr = IGAMComputeSystem(iga,A,b);CHKERRQ(ierr);
  ierr = IGAMComputeMatrix(iga,A);CHKERRQ(ierr);
  
  //----------------------------------------------//
  // Print data of the discretization
  for (j=0; j<3; j++)
  {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"%sdiscretization: \n",prefix[j]);CHKERRQ(ierr);
    for (i=0; i<2; i++) {
    ierr = PetscPrintf(PETSC_COMM_WORLD," - Axis %D: basis=%s[%D] rule=%s[%D] periodic=%d nnp=%D nel=%D\n", i,
                                  IGABasisTypes[iga->f_iga[j]->basis[i]->type],iga->f_iga[j]->axis[i]->p,
                                  IGARuleTypes[iga->f_iga[j]->rule[i]->type],iga->f_iga[j]->rule[i]->nqp,
                                  (int)iga->f_iga[j]->axis[i]->periodic,iga->f_iga[j]->node_sizes[i],iga->f_iga[j]->elem_sizes[i]);CHKERRQ(ierr);
    }
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD,"geo_discretization: \n");CHKERRQ(ierr);
  for (i=0; i<2; i++) {
  ierr = PetscPrintf(PETSC_COMM_WORLD," - Axis %D: basis=%s[%D] rule=%s[%D] periodic=%d nnp=%D nel=%D\n",i,
                                IGABasisTypes[iga->g_iga->basis[i]->type],iga->g_iga->axis[i]->p,
                                IGARuleTypes[iga->g_iga->rule[i]->type],iga->g_iga->rule[i]->nqp,
                                (int)iga->g_iga->axis[i]->periodic,iga->g_iga->node_sizes[i],iga->g_iga->elem_sizes[i]);CHKERRQ(ierr);
  }

  //----------------------------------------------//
  // Factorization of the Matrix using LU factorization
  Mat LU;
  KSP ksp; PC pc;
  ierr = IGAMCreateKSP(iga,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  // Direct solver
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
  // Solver type
  PetscInt MUMPS_type   = strcmp(user.solver_type,"MUMPS");
  PetscInt PARDISO_type = strcmp(user.solver_type,"PARDISO");
  if ( (MUMPS_type != 0) && (PARDISO_type != 0) ) SETERRQ1(PETSC_COMM_WORLD,1,"Incorrect direct solver type",NULL);
  if (MUMPS_type == 0)
  {
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);CHKERRQ(ierr);
    ierr = PCFactorSetUpMatSolverType(pc);CHKERRQ(ierr); /* call MatGetFactor() to create LU */
    ierr = PCFactorGetMatrix(pc,&LU);CHKERRQ(ierr);
    ierr = MatMumpsSetIcntl(LU,7,5);CHKERRQ(ierr);
    ierr = MatMumpsSetIcntl(LU,4,3);CHKERRQ(ierr);
  }
  else if (PARDISO_type == 0)
  {
    SETERRQ1(PETSC_COMM_WORLD,1,"Ali's computer does not have Pardiso",NULL);
    //ierr = PCFactorSetMatSolverType(pc,MATSOLVERMKL_PARDISO);CHKERRQ(ierr);
    //ierr = PCFactorSetUpMatSolverType(pc);CHKERRQ(ierr); /* call MatGetFactor() to create LU */
    //ierr = PCFactorGetMatrix(pc,&LU);CHKERRQ(ierr);
    //ierr = MatMkl_PardisoSetCntl(LU,68,1);CHKERRQ(ierr);
  }
  // Setup solver
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetUp(ksp);CHKERRQ(ierr);

  //----------------------------------------------//
  // Magnitude sources
  user.srcmag[0] = 0;
  user.srcmag[1] = 0;
  user.srcmag[2] = 1;
  // Source localization
  user.srcloc[0] = 0;
  user.srcloc[1] = 0;
  user.srcloc[2] = 0;
  // Identify the element what contains the source
  ierr = Get_ele_src(geom_lin,&user);CHKERRQ(ierr);
  // Compute RHS
  ierr = IGAMComputeVector(iga,b);CHKERRQ(ierr);

  //----------------------------------------------//
  // Compute solution using previous computed RHS
  ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);

  //----------------------------------------------//
  // Compute Point value
  //
  // To compute a point value we use 
  // ierr   = GetRValue(iga,fld,x,point,u,&user);CHKERRQ(ierr);
  // where x is the solution of FE, point contains the coordinates of the point, and u is the point value

  //----------------------------------------------//
  // Check solution (Compare with the analytical solution)
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&b);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  //
  PetscScalar u[2];
  PetscReal point[2];
  //
  i = 0;
  j = 0;
  //
  char filename[PETSC_MAX_PATH_LEN];
  //PetscMPIInt    rank;
  FILE           *file = NULL;
  // Create file name
  //MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  PetscSNPrintf(filename,sizeof(filename),"Hzz/Hzz_N%d_p%d_l%d_bt%d_sg%d_Pos%d_Src%d.txt",nel,ord,l,user.beta,sg,i+1,j+1);
  // open file and write header
  file = fopen(filename,"w");
  if (!file) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SYS,"Cannot open file = %s \n",filename);
  PetscFPrintf(PETSC_COMM_WORLD,file," Point(x,z) | source(x,z) | Souce magnitude(x,y,z) | Real(u) | Imag(u) | Mag(u)\n");
  // Write results
  i=-1;
  do
  {
    i+=1;
    point[0] = 0.0;
    point[1] = 0.1*i;
    ierr   = GetRValue(iga,fld,x,point,u);CHKERRQ(ierr);
    //
    PetscReal absHz_u;
    absHz_u = sqrt(pow(PetscRealPart(u[1]),2)+pow(PetscImaginaryPart(u[1]),2));
    ierr = PetscPrintf(PETSC_COMM_WORLD," ->       |Hz_u|(%f,%f)=%e\n",point[0],point[1],absHz_u);CHKERRQ(ierr);
    //
    PetscFPrintf(PETSC_COMM_WORLD,file,"%f %f | %f %f | %f %f %f | %e | %e | %e\n",point[0],point[1],
                                                                                   user.srcloc[0],user.srcloc[2],
                                                                                   user.srcmag[0],user.srcmag[1],user.srcmag[2],
                                                                                   PetscRealPart(u[1]),
                                                                                   PetscImaginaryPart(u[1]),
                                                                                   absHz_u);
  } while (point[1]<0.5*user.Ly); // This condition assumes that the domain is square (cube)
  // Close file
  if (file) {
    fclose(file);
    file = NULL;
  }

  //----------------------------------------------//
  // Save solution to print vtks
  i=0;
  j=0;
  Vec *vecs;
  char x_mag[256],y_mag[256],z_mag[256];
  sprintf(x_mag,"results/HX_N%d_p%d_l%d_bt%d_sg%d_Pos%d_Src%d.dat",nel,ord,l,user.beta,sg,i+1,j+1);
  sprintf(y_mag,"results/HY_N%d_p%d_l%d_bt%d_sg%d_Pos%d_Src%d.dat",nel,ord,l,user.beta,sg,i+1,j+1);
  sprintf(z_mag,"results/HZ_N%d_p%d_l%d_bt%d_sg%d_Pos%d_Src%d.dat",nel,ord,l,user.beta,sg,i+1,j+1);
  ierr = IGAMUnpackVec(iga,x,&vecs);CHKERRQ(ierr);
  {
    ierr = IGAWriteVec(field[0],vecs[0],x_mag);CHKERRQ(ierr);
    ierr = IGAWriteVec(field[1],vecs[1],y_mag);CHKERRQ(ierr);
    ierr = IGAWriteVec(field[2],vecs[2],z_mag);CHKERRQ(ierr);
  }
  ierr = IGAMRepackVec(iga,x,&vecs);CHKERRQ(ierr);

  //----------------------------------------------//
  // Cleanup
  //ierr = MatDestroy(&A);CHKERRQ(ierr);
  //ierr = VecDestroy(&b);CHKERRQ(ierr);
  //ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);

  return 0;
}
